<?php

/**
 * @file
 * Builds profile-specific register, login, and password forms.
 */

/**
 * Page callback: Displays a user login form.
 *
 * Path: [path-setting]/login
 *
 * @param object $profiles
 *   Object containing single row from profile2_regpath_get_profiles() database
 *   result.
 *
 * @see profile2_regpath_menu()
 */
function _profile2_regpath_user_login($profiles) {
  module_load_include('pages.inc', 'user', 'user');
  $output = user_page();
  _profile2_regpath_set_title($profiles, 'login_title');
  return $output;
}

/**
 * Page callback: Displays a user registration form.
 *
 * Path: [path-setting]/register
 *
 * @param object object $profiles
 *   Object containing single row from profile2_regpath_get_profiles() database
 *   result.
 *
 * @see profile2_regpath_menu()
 */
function _profile2_regpath_user_register($profiles) {
  $code = arg(2);
  module_load_include('pages.inc', 'user', 'user');
  $output = drupal_get_form('user_register_form');
  $expireFlag = 0;
  $record = getBetaDetails($code);
  $message = "";
  if($record === FALSE){

       $expireFlag = 1;
       $message = "<p class='betaMessage'>You dont have valid token for accessing this page.</p>" ;
     }else{
       $datetime = $record->mailSentDateTime;
       $hour =0;$mins = 0; $secs = 0; $year = 0; $month = 0; $day = 0;
       if(strpos($datetime,' ')){

         $dateTimeExplode = explode(' ', $datetime); 

         $time = $dateTimeExplode[1];
         $date = $dateTimeExplode[0];
         if(strpos($time,':')){
           $timeExplode = explode(':', $time);
           $hour = $timeExplode[0];
           $mins = $timeExplode[1];
           $secs = $timeExplode[2];
         }else{
           $expireFlag = 1; 
           $message = "<p class='betaMessage'>Your token has expired.</p>" ;
         }

         if(strpos($date,'-')){
           $dateExplode = explode('-', $date);
           $year = $dateExplode[0];
           $month = $dateExplode[1];
           $day = $dateExplode[2];
         }else{
           $expireFlag = 1; 
           $message = "<p class='betaMessage'>Your token has expired.</p>" ;
         }
       }
       if($expireFlag == 0){
         $ts = mktime($hour,$mins,$secs,$month,$day,$year);
         $ts = strtotime('+1 day', $ts);
         if($ts > REQUEST_TIME){
            $expireFlag = 0; 
         }else{
           $expireFlag = 1; 
           $message = "<p class='betaMessage'>Your token has expired.</p>" ;
         }
       }
     }
     if($expireFlag == 1){
         unset($output);
          $output['meesage']['#markup'] = $message ;  
       // drupal_set_message($message)  ;  

     }else{
       $output['field_first_name']['und'][0]['value']['#value'] = $record->businessname;
       $output['field_first_name']['und'][0]['value']['#maxlength'] = 100;
       $output['account']['mail']['#value'] = $record->email;
       $output['account']['mail']['#attributes'] = array('readonly' => 'readonly');
       $output['profile_vendor_profile']['field_website']['und'][0]['url']['#value'] = $record->website;
     } 
  
  _profile2_regpath_set_title($profiles, 'register_title');
  return $output;
}
function _profile2_regpath_user_register_modal($profiles,$js) {
  //print_r($profiles) ;
  if(!$js){
    module_load_include('pages.inc', 'user', 'user');
    $output = drupal_get_form('user_register_form');
    _profile2_regpath_set_title($profiles, 'register_title');
    return $output;
  }
  
  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    'title' => t('Create new account'),
    'ajax' => TRUE,
  );
  $output = ctools_modal_form_wrapper('user_register_form', $form_state);
  _profile2_regpath_set_title($profiles, 'register_title');
  print ajax_render($output);
  
  
}
/**
 * Page callback: Displays the forgot password form.
 *
 * Path: [path-setting]/password
 *
 * @param object $profiles
 *   Object containing single row from profile2_regpath_get_profiles() database
 *   result.
 *
 * @see profile2_regpath_menu()
 */
function _profile2_regpath_user_password($profiles) {
  module_load_include('pages.inc', 'user', 'user');
  $output = drupal_get_form('user_pass');
  _profile2_regpath_set_title($profiles, 'password_title');
  return $output;
}

/**
 * Sets page title for registration, login, and forgot password pages.
 *
 * @param object $profiles
 *   Object containing single row from profile2_regpath_get_profiles() database
 *   result.
 *
 * @param string $key
 *   Array key for 'misc' array. This will determine the title settings.
 */
function _profile2_regpath_set_title($profiles, $key) {
  // Look for custom title in foremost profile, according to weight.
  if (isset($profiles[0]->misc) && $misc = unserialize($profiles[0]->misc)) {
    if (array_key_exists($key, $misc)) {
      $title = $misc[$key];
    }
  }
  // Set default title.
  else {
    $title = 'User account';
  }
  // Set title. See http://drupal.org/node/1800116 for use of t().
  drupal_set_title(t($title));
}