jQuery(document).ready(function() {
  // Handler for .ready() called.
  jQuery(":input[name='service_worldwide']").click(function(){
  	var value = jQuery(":input[name='service_worldwide']:checked").val();
  	
  	if(value == 'N'){
  		jQuery("#serviceCountry").show();
  		jQuery("#serviceState").show();
  		jQuery("#serviceLocationsDiv").show();
  	}else if(value == 'Y'){
  		jQuery("#serviceCountry").hide();
  		jQuery("#service-country-select option:selected").removeAttr('selected');
  		doChosen("#service-country-select");
  		jQuery("#serviceState").hide();
  		jQuery("#service-state-select option:selected").removeAttr('selected');
  		doChosen("#service-state-select");
  		jQuery("#serviceLocationsDiv").hide();
  		jQuery("#service-city-select option:selected").removeAttr('selected');
  		doChosen("#service-city-select");
  	}
  });

  var value = jQuery(":input[name='service_worldwide']:checked").val();
  
  if(value != ""){
  	jQuery(":input[name='service_worldwide']:checked").trigger("click");
  }
});