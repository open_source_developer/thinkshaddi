<?php

function image_service_index($page,$bundle,$category,$tag,$color) {
  global $base_url;

  $limit = 10;
  $start = $page*$limit;
  $images = array();
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
  		->entityCondition('bundle', array('bundle_images','board_videos'),'IN')
  		->propertyCondition('status', 1)
      ->propertyOrderBy('changed','DESC');

  if($bundle){

    $query->fieldCondition('field_bundle','target_id',$bundle,'=');
  }
  if($category){

    $query->fieldCondition('field_category','tid',$category,'=');
  }
  if($tag){
    $query->fieldCondition('field_image_tags','tid',$tag,'=');
  }
  if($color){
    $query->fieldCondition('field_colors','tid',$color,'=');
  }    
		$query->range($start, $limit);
  $result = $query->execute();

 // print_r($result);
  if (isset($result['node'])) {
    $images_nids = array_keys($result['node']);
    $imageNodes = entity_load('node', $images_nids);
    //print_r($imageNodes);
    $j=0;
    foreach($imageNodes as $key => $value){
    	$images[$j]['id'] = $value->nid; 
    	$images[$j]['title'] = $value->title;
      if($value->type == "bundle_images"){
        $images[$j]['type'] = "image";
      }elseif($value->type == "board_videos"){
        $images[$j]['type'] = "video";
      }
    	$desc = "";
      $images[$j]['url'] = $base_url."/".drupal_lookup_path('alias',"node/".$value->nid);
    	if(isset($value->field_image_description['und'][0]['value'])){
		    $desc = $value->field_image_description['und'][0]['value'];
    	} 
      if(strlen($desc) > 100){
        $desc = substr($desc,0,100)."....";
      }
    	$images[$j]['description'] = $desc;
    	$categoryCount = count($value->field_category['und']);
    	$categoryArray = '';
      $catIndex = 0;
    	for($i=0;$i<$categoryCount;$i++){
    		$term =  taxonomy_term_load($value->field_category['und'][$i]['tid']);
    		if(isset($term->name)){
				  
    			$images[$j]['category'][$catIndex]['name'] = $term->name;
          $images[$j]['category'][$catIndex]['url'] =  $base_url."/".drupal_lookup_path('alias',"taxonomy/term/".$value->field_category['und'][$i]['tid']);
          $catIndex ++;
    		}
    		
    	}
    	if($categoryCount == 0){
        $images[$j]['category'] = "";
      }
    	$images[$j]['likes'] = 0 ;
    	$images[$j]['favorites'] = 0 ;
    	$user=user_load($value->uid);
      $firstName = "";
      $lastName = "";
      if(isset($user->field_first_name['und'][0]['value'])){
          $firstName = $user->field_first_name['und'][0]['value'];
      }
      if(isset($user->field_last_name ['und'][0]['value'])){
          $lastName = $user->field_last_name ['und'][0]['value'];
      }
    	if(isset($user->name)){
			  $images[$j]['vendor']['name'] = $firstName." ".$lastName;
        $images[$j]['vendor']['url'] =  $base_url."/".drupal_lookup_path('alias',"user/".$value->uid); ;
    	}
    	
    	$images[$j]['img'] = file_create_url($value->field_image_path['und'][0]['uri']);
    	$j++;

    }
  }
  $url ='';
   $next = 0 ;
  if(count($images) >= $limit){
    $next = 1;
  }
  return array('result'=>$images,'next'=>$next);
}

function bundle_service_index($page){
  //$json = views_embed_view('categories','services_1');
  //$array = json_decode($json);
  //print_r($array);
  //echo "here";
  $next = 0;
  $bundles = vendor_board(0,$page);
  if(count($bundles) >= 10){
    $next = 1;
  }else{
    $next = 1;
  }
  return array('result'=>$bundles,'next'=>$next);
}

function country_filter_index($country){
  $cityOptions = array();
  $stateOptions =array();
  $countryPlain = check_plain($country);
  if(!empty($countryPlain)){
     if(strpos($countryPlain, ',') === FALSE){ 
       $value[] = $countryPlain;
     }else{
       
       $value = explode(',', $countryPlain);
     }

     $taxonomyData = taxonomy_vocabulary_machine_name_load('city');
    //dsm($taxonomyData);
    $vid = $taxonomyData->vid;
    $query = new EntityFieldQuery;
    $result = $query->entityCondition('entity_type', 'taxonomy_term','=')
          ->propertyCondition('vid', $vid)
          ->fieldCondition('field_country','tid',$value,'IN')
      ->execute();
      if(isset($result['taxonomy_term'])){
     foreach($result['taxonomy_term'] as $term) {
        $key = $term->tid;
        $pterm = taxonomy_term_load($key);
        $cityOptions[$pterm->tid] = check_plain($pterm->name);
     }
   }

   $taxonomyData = taxonomy_vocabulary_machine_name_load('state');
    //dsm($taxonomyData);
    $vid = $taxonomyData->vid;
    $query = new EntityFieldQuery;
    $result = $query->entityCondition('entity_type', 'taxonomy_term','=')
          ->propertyCondition('vid', $vid)
          ->fieldCondition('field_country','tid',$value,'IN')
      ->execute();
      if(isset($result['taxonomy_term'])){
     foreach($result['taxonomy_term'] as $term) {
        $key = $term->tid;
        $pterm = taxonomy_term_load($key);
        $stateOptions[$pterm->tid] = check_plain($pterm->name);
     }
   }


  }
    return array("cityOptions"=>$cityOptions,"stateOptions"=>$stateOptions);
}

function state_filter_index($state){

  $cityOptions = array();
  
  $statePlain = check_plain($state);
  if(!empty($statePlain)){
     if(strpos($statePlain, ',') === FALSE){ 
       $value[] = $statePlain;
     }else{
       
       $value = explode(',', $statePlain);
     }

     $value[] = "";

     $taxonomyData = taxonomy_vocabulary_machine_name_load('city');
    //dsm($taxonomyData);
    $vid = $taxonomyData->vid;
    $query = new EntityFieldQuery;
    $result = $query->entityCondition('entity_type', 'taxonomy_term','=')
          ->propertyCondition('vid', $vid)
          ->fieldCondition('field_state','tid',$value,'IN')
      ->execute();
      if(isset($result['taxonomy_term'])){
     foreach($result['taxonomy_term'] as $term) {
        $key = $term->tid;
        $pterm = taxonomy_term_load($key);
        $cityOptions[$pterm->tid] = check_plain($pterm->name);
     }
   }

  


  }
    return array("option"=>$cityOptions);
}

function subcat_service_index($category){
  $options = array();
  $categoryPlain = check_plain($category);
  if(!empty($categoryPlain)){
     if(strpos($categoryPlain, ',') === FALSE){ 
       $value[] = $categoryPlain;
     }else{
       
       $value = explode(',', $categoryPlain);
     }

     $taxonomyData = taxonomy_vocabulary_machine_name_load('sub_cateogory');
    //dsm($taxonomyData);
    $vid = $taxonomyData->vid;
    $query = new EntityFieldQuery;
    $result = $query->entityCondition('entity_type', 'taxonomy_term','=')
          ->propertyCondition('vid', $vid)
          ->fieldCondition('field_category','tid',$value,'IN')
      ->execute();
      if(isset($result['taxonomy_term'])){
     foreach($result['taxonomy_term'] as $term) {
        $key = $term->tid;
        $pterm = taxonomy_term_load($key);
        $options[$pterm->tid] = check_plain($pterm->name);
     }
      asort($options);
      
   }
  }
  return array("option"=>$options);

}
