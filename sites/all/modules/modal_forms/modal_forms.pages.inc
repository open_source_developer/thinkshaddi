<?php
/**
 * @file
 * Page callbacks for the modal_forms module.
 */

function modal_forms_vendorviewdeals($js=NULL,$nid){
  $refNode = node_load($nid);
  $title ="";
  $content = "";
  if($refNode){
    $dealNode = query_data_get_deal($nid,$refNode->type,1);
    if ($js) {
      // Required includes for ctools to work:
      ctools_include('modal');
      ctools_include('ajax');
    }
    
    
    // Drupal 7 requires a render of the node object in order to obtain a string.
    // Note that I am able to customize the fields by using the "Teaser" display mode 
    // under admin/structure/types.
    $title = $dealNode->title;
    $content = "<hr/><div><p>".nl2br($dealNode->field_deal_description['und'][0]['value'])."</p></div>";
  }
 
  return ctools_modal_render($title, $content) ;
}


function modal_forms_vendoroptions($js= NULL,$nid){
  if (!$js) {
    return drupal_get_form('vendor_options_form',$nid);
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    // 'title' => t('Log in'),
    'ajax' => TRUE,
  );
  $form_state['build_info']['args']['#nid'] = $nid;
  $output = ctools_modal_form_wrapper('vendor_options_form', $form_state,$nid);
  
  if (!empty($form_state['executed'])) {
    $output = array();
    // Close pop up.
    $output[] = ctools_modal_command_dismiss();
    $output[] = ctools_ajax_command_reload(); 
  }
  print ajax_render($output);
}

function modal_forms_vendordeals($js= NULL,$nid){
  if (!$js) {
    return drupal_get_form('vendor_deals_form',$nid);
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    // 'title' => t('Log in'),
    'ajax' => TRUE,
  );
  $form_state['build_info']['args']['#nid'] = $nid;
  $output = ctools_modal_form_wrapper('vendor_deals_form', $form_state,$nid);
  
  if (!empty($form_state['executed'])) {
    $output = array();
    // Close pop up.
    $output[] = ctools_modal_command_dismiss();
    $output[] = ctools_ajax_command_reload(); 
  }
  print ajax_render($output);
}
/**
 * A modal user images callback.
 */
function modal_forms_images($js = NULL,$path) {
  // Fall back if $js is not set.
  global $base_url;
  global $user;
  $type = $path;
  
  $node = (object) array(
    'uid' => $user->uid,  
    'name' => (isset($user->name) ? $user->name : ''),  
    'type' => $type,  
    'language' => LANGUAGE_NONE);
  $form_state['build_info']['args'] = array($node);
  form_load_include($form_state, 'inc', 'node', 'node.pages');
  if (!$js) {
    $form = drupal_get_form($type.'_node_form',$node);
    return $form;
  }
   
  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    'ajax' => TRUE,
  );
  
  $form_state['build_info']['args'] = array($node);
  $output = ctools_modal_form_wrapper($type.'_node_form', $form_state);
  
  if (!empty($form_state['executed'])) {
    $output = array();
    // Close pop up.
    $output[] = ctools_modal_command_dismiss();
    if($form_state['node']->type == "bundle_images"){
      $message = drupal_get_messages($type = NULL, $clear_queue = TRUE);
      $board = "";
      if(isset($form_state['node']->field_bundle['und'][0]['target_id'])){
         $node = node_load($form_state['node']->field_bundle['und'][0]['target_id']);
         if($node){
           $board = $node->title;
         }
      }
      $message = "A new image has been added to your $board board.";
      drupal_set_message($message);
      $output[] = ctools_modal_command_dismiss();
     $url =  $base_url."/".drupal_lookup_path('alias',"node/".$form_state['node']->nid);
     $output[] = ctools_ajax_command_redirect($url);
    }elseif($form_state['node']->type == "bundles"){
       $message = drupal_get_messages($type = NULL, $clear_queue = TRUE);
       $message = "Your Board ".$form_state['node']->title." has been created.";
      drupal_set_message($message);
        $output[] = ctools_modal_command_dismiss();
        $url =  $base_url."/".drupal_lookup_path('alias',"node/".$form_state['node']->nid);
        
        $output[] = ctools_ajax_command_redirect($url);
    }elseif($form_state['node']->type == "board_videos"){
      $message = drupal_get_messages($type = NULL, $clear_queue = TRUE);
      $board = "";
      if(isset($form_state['node']->field_bundle['und'][0]['target_id'])){
         $node = node_load($form_state['node']->field_bundle['und'][0]['target_id']);
         if($node){
           $board = $node->title;
         }
      }
       $message = "A new video has been added to your $board board.";
       drupal_set_message($message);
       $output[] = ctools_modal_command_dismiss();
       $url =  $base_url."/".drupal_lookup_path('alias',"node/".$form_state['node']->nid);
       $output[] = ctools_ajax_command_redirect($url);
    }
    
  }elseif($form_state['submitted']){
      $output[] = ajax_command_invoke(NULL,"scrollToError",array());
  }
  print ajax_render($output);
}

function modal_forms_selectionpage($js = NULL) {
  global $base_url;
  if ($js) {
    // Required includes for ctools to work:
    ctools_include('modal');
    ctools_include('ajax');
  }
  
  
  // Drupal 7 requires a render of the node object in order to obtain a string.
  // Note that I am able to customize the fields by using the "Teaser" display mode 
  // under admin/structure/types.
  $contents = "<hr/><ul class='uploadTabs'><li><a href='". $base_url."/modal_forms/nojs/node/add/bundle_images' class='imageUpload ctools-use-modal ctools-modal-modal-popup-large'>Upload Image</a></li><li><a href='". $base_url."/modal_forms/nojs/node/add/board_videos' class='videoUpload ctools-use-modal ctools-modal-modal-popup-large'>Upload Video</a></li></ul>";
  return ctools_modal_render('Select', $contents) ;
}

function modal_forms_edit($js = NULL,$nid){
   global $user;
   global $base_url;
   $nodeObj = node_load($nid);
   //dsm($nodeObj);
   $type = $nodeObj->type;
   $form_state['build_info']['args'] = array($nodeObj);
  form_load_include($form_state, 'inc', 'node', 'node.pages');
 
   if (!$js) {
     $form = drupal_get_form($type.'_node_form',$form_state);

     return $form;
   }
    ctools_include('modal');
    ctools_include('ajax');
    $form_state = array(
      'title' => t('Edit'),
      'ajax' => TRUE,
    );
    
    $form_state['build_info']['args'] = array($nodeObj);
    $output = ctools_modal_form_wrapper($type.'_node_form', $form_state);

    if (!empty($form_state['executed'])) {
      //dsm($form_state);
      $output = array();
      // Close pop up.
      //dsm($form_state['node']);
      if($form_state['node']->type == "bundle_images"){
         if($form_state['clicked_button']['#value'] == "Delete Image"){
          node_delete($form_state['node']->nid);
          drupal_set_message(t('Your image has been deleted.'));
          $output[] = ctools_ajax_command_redirect($base_url);
        }elseif($form_state['clicked_button']['#value'] == "Save Image"){
          $output[] = ctools_modal_command_dismiss();
          $url =  $base_url."/".drupal_lookup_path('alias',"node/".$form_state['node']->nid);
          $output[] = ctools_ajax_command_redirect($url);
        }else{
          $output[] = ctools_modal_command_dismiss();
          $output[] = ctools_ajax_command_reload(); 
        }
         
      }elseif($form_state['node']->type == "board_videos"){
         if($form_state['clicked_button']['#value'] == "Delete Video"){
          node_delete($form_state['node']->nid);
          drupal_set_message(t('Your video has been deleted.'));
          $output[] = ctools_ajax_command_redirect($base_url);
        }elseif($form_state['clicked_button']['#value'] == "Save Video"){
          $output[] = ctools_modal_command_dismiss();
          $url =  $base_url."/".drupal_lookup_path('alias',"node/".$form_state['node']->nid);
          $output[] = ctools_ajax_command_redirect($url);
        }else{
          $output[] = ctools_modal_command_dismiss();
          $output[] = ctools_ajax_command_reload(); 
        }
         
      }elseif($form_state['node']->type == "bundles"){
        if($form_state['clicked_button']['#value'] == "Save Board"){
          $output[] = ctools_modal_command_dismiss();
          $url =  $base_url."/".drupal_lookup_path('alias',"node/".$form_state['node']->nid);
          if($cache = cache_get('bundles')){
   
            $bundlesCache = $cache->data;
            if(isset($bundlesCache[$form_state['node']->nid])){
             
              $bundlesCache[$form_state['node']->nid]['title'] = $form_state['node']->title;
              $bundlesCache[$form_state['node']->nid]['desc'] = $form_state['node']->field_description['und'][0]['value'];
              cache_set('bundles', $bundlesCache, 'cache',$cache->expire);
            }
          }
          $output[] = ctools_ajax_command_redirect($url);
        }else if($form_state['clicked_button']['#value'] == "Delete Board"){
          node_delete($form_state['node']->nid);
          drupal_set_message(t('Bundle @title Deleted',array('@title'=>$form_state['node']->title)));
          $output[] = ctools_ajax_command_redirect($base_url);
        }
      }
      
      
    }elseif($form_state['submitted']){
      $output[] = ajax_command_invoke(NULL,"scrollToError",array());
    }
    print ajax_render($output);
}


/**
 * A modal user register callback fo profile2 profile.
 */
function modal_forms_profile_register($js = NULL,$path,$code) {
  module_load_include('pages.inc', 'user', 'user');
  if (!$js) {
     $form = drupal_get_form('user_register_form');
    // dsm($form);
     $expireFlag = 0;
     $record = getBetaDetails($code);
     
     
     if($record === FALSE){

       $expireFlag = 1;
       $message = "<p class='betaMessage'>You dont have valid token for accessing this page.</p>" ;
     }else{
       $datetime = $record->mailSentDateTime;
       $hour =0;$mins = 0; $secs = 0; $year = 0; $month = 0; $day = 0;
       if(strpos($datetime,' ')){

         $dateTimeExplode = explode(' ', $datetime); 

         $time = $dateTimeExplode[1];
         $date = $dateTimeExplode[0];
         if(strpos($time,':')){
           $timeExplode = explode(':', $time);
           $hour = $timeExplode[0];
           $mins = $timeExplode[1];
           $secs = $timeExplode[2];
         }else{
           $expireFlag = 1; 
           $message = "<p class='betaMessage'>Your token has expired.</p>" ;
         }

         if(strpos($date,'-')){
           $dateExplode = explode('-', $date);
           $year = $dateExplode[0];
           $month = $dateExplode[1];
           $day = $dateExplode[2];
         }else{
           $expireFlag = 1; 
           $message = "<p class='betaMessage'>Your token has expired.</p>" ;
         }
       }
       if($expireFlag == 0){
         $ts = mktime($hour,$mins,$secs,$month,$day,$year);
         $ts = strtotime('+1 day', $ts);
         if($ts > REQUEST_TIME){
            $expireFlag = 0; 
         }else{
           $expireFlag = 1; 
           $message = "<p class='betaMessage'>Your token has expired.</p>" ;
         }
       }
     }
     if($expireFlag == 1){
       unset($form);
       $form['meesage']['#markup'] = $message ;  
     }else{
       
       $form['account']['mail']['#value'] = $record->email;
       $form['account']['mail']['#attributes'] = array('readonly' => 'readonly');
       $form['profile_vendor_profile']['field_website']['und'][0]['url']['#value'] = $record->website;
     } 
     
     //$form['account']['mail']['#value'] = 
     return $form;
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    // 'title' => t('Create new account'),
    'ajax' => TRUE,
  );
  $output = ctools_modal_form_wrapper('user_register_form', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();
    ctools_add_js('ajax-responder');
    if (isset($_GET['destination'])) {
      $output[] = ctools_ajax_command_redirect($_GET['destination']);
    }
    elseif(module_exists('login_destination')) {
      $destination = login_destination_get_destination('login');
      $output[] = ctools_ajax_command_redirect($destination['path']);
    }
    else {
      $output[] = ctools_ajax_command_reload();
    }
  }
  print ajax_render($output);
}


function modal_forms_change_password($js){
  if (!$js) {
    return drupal_get_form('change_password_form');
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    // 'title' => t('Log in'),
    'ajax' => TRUE,
  );
  $output = ctools_modal_form_wrapper('change_password_form', $form_state);
  
  if (!empty($form_state['executed'])) {
    $output = array();
    // Close pop up.
    $output[] = ctools_modal_command_dismiss();
    $output[] = $command[] = ajax_command_html(".passwordSucess",'Your Password Changed Sucessfully.');
  }
  print ajax_render($output);

}



/*
 * A modal user login callback.
 */
function modal_forms_login($js = NULL) {
  // Fall back if $js is not set.
  if (!$js) {
    return drupal_get_form('user_login');
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
     
    'ajax' => TRUE,
  );

  $output = ctools_modal_form_wrapper('user_login', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();
    ctools_add_js('ajax-responder');
    $output[] = ctools_modal_command_dismiss(t('Login success'));
    if (isset($_GET['destination'])) {
      $output[] = ctools_ajax_command_redirect($_GET['destination']);
    }
    elseif(module_exists('login_destination')) {
      $destination = login_destination_get_destination('login');
      $output[] = ctools_ajax_command_redirect($destination['path']);
    }
    else {
      $output[] = ctools_ajax_command_reload();
    }
  }
  print ajax_render($output);
}

/**
 * A modal user register callback.
 */
function modal_forms_register($js = NULL) {
  // Fall back if $js is not set.
  if (!$js) {
    return drupal_get_form('user_register_form');
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
     'title' => t('Sign Up for a Free ThinkShaadi Account'),
    'ajax' => TRUE,
  );
  $output = ctools_modal_form_wrapper('user_register_form', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();
    ctools_add_js('ajax-responder');
    if (isset($_GET['destination'])) {
      $output[] = ctools_ajax_command_redirect($_GET['destination']);
    }
    elseif(module_exists('login_destination')) {
      $destination = login_destination_get_destination('login');
      $output[] = ctools_ajax_command_redirect($destination['path']);
    }
    else {
      $output[] = ctools_ajax_command_reload();
    }
  }
  print ajax_render($output);
}


/**
 * A modal user password callback.
 */
function modal_forms_password($js = NULL) {
  module_load_include('inc', 'user', 'user.pages');

  // Fall back if $js is not set.
  if (!$js) {
    return drupal_get_form('user_pass');
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    // 'title' => t('Request new password'),
    'ajax' => TRUE,
  );
  $output = ctools_modal_form_wrapper('user_pass', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();
    ctools_add_js('ajax-responder');
    if (isset($_GET['destination'])) {
      $output[] = ctools_ajax_command_redirect($_GET['destination']);
    }
    else {
      $output[] = ctools_ajax_command_reload();
    }
  }
  print ajax_render($output);
}

/**
 * A modal contact callback.
 */
function modal_forms_contact($js = NULL) {
  module_load_include('inc', 'contact', 'contact.pages');

  // Fall back if $js is not set.
  if (!$js) {
    return drupal_get_form('contact_site_form');
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    // 'title' => t('Contact'),
    'ajax' => TRUE,
  );
  $output = ctools_modal_form_wrapper('contact_site_form', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();
    ctools_add_js('ajax-responder');
    if (isset($_GET['destination'])) {
      $output[] = ctools_ajax_command_redirect($_GET['destination']);
    }
    else {
      $output[] = ctools_ajax_command_reload();
    }
  }
  print ajax_render($output);
}

/**
 * A modal comment callback.
 */
function modal_forms_comment_reply($js = NULL, $node, $pid = NULL) {
  $output = array();
  $comment = array(
    'pid' => $pid,
    'nid' => $node->nid,
  );

  if (!$js) {
    return drupal_get_form('comment_node_' . $node->type . '_form', (object) $comment);
  }

  ctools_include('modal');
  ctools_include('ajax');

  $form_state = array(
    'build_info' => array(
      'args' => array(
        (object) $comment,
      ),
    ),
    // 'title' => t('Comment'),
    'ajax' => TRUE,
    're_render' => FALSE,
    'no_redirect' => TRUE,
  );

  // Should we show the reply box?
  if ($node->comment != COMMENT_NODE_OPEN) {
    drupal_set_message(t('This discussion is closed: you can\'t post new comments.'), 'error');
    drupal_goto('node/' . $node->nid);
  }
  else {
    $output = drupal_build_form('comment_node_' . $node->type . '_form', $form_state);
    // Remove output bellow the comment.
    unset($output['comment_output_below']);
  }

  if (!$form_state['executed'] || $form_state['rebuild']) {
    $output = ctools_modal_form_render($form_state, $output);
  }
  else {
    // We'll just overwrite the form output if it was successful.
    $output = array();
    // @todo: Uncomment once http://drupal.org/node/1587916 is fixed.
    //if (is_array($form_state['redirect'])) {
    //  list($path, $options) = $form_state['redirect'];
    //  $output[] = ctools_ajax_command_redirect($path, 0, $options);
    //}
    if (isset($_GET['destination'])) {
      $output[] = ctools_ajax_command_redirect($_GET['destination']);
    }
    else {
      $output[] = ctools_ajax_command_reload();
    }
  }

  print ajax_render($output);
}

/**
 * Modal display of the node's webform.
 *
 * @param $node
 *   A node object.
 */
function modal_forms_view_webform($js = NULL, $node) {
  $output = array();

  // Fall back if $js is not set.
  if (!$js) {
    return drupal_get_form('webform_client_form_' . $node->nid, $node, FALSE);
  }

  ctools_include('modal');
  ctools_include('ajax');

  if (empty($node->webform['components'])) {
    // No webform or no components.
    $output[] = ctools_modal_command_display(t('Webform'), t('No webform found.'));
    print ajax_render($output);
    exit;
  }

  // Get webform defaults.
  $title = check_plain($node->title);
  $text = check_markup($node->webform['confirmation'], $node->webform['confirmation_format'], '', TRUE);
  $form_state = array(
    'title' => $title,
    'ajax' => TRUE,
  );

  // Prevent webform redirect.
  $GLOBALS['conf']['webform_blocks']['client-block-' . $node->nid]['pages_block'] = 1;
  $node->webform_block = TRUE;

  // Pass required parameters.
  $form_state['build_info']['args'] = array($node, FALSE);
  $output = ctools_modal_form_wrapper('webform_client_form_' . $node->nid, $form_state);

  if (!empty($form_state['executed'])) {
    if (!isset($form_state['storage'])) {
      ctools_add_js('ajax-responder');
      $output[] = ctools_modal_command_display($title, $text . ctools_ajax_text_button(t('Close'), 'modal_forms/nojs/dismiss', t('Close')));
      // @todo Add support for redirect. Require some magic.
      // Copied from webform_client_form_submit().
      // $redirect_url = trim($node->webform['redirect_url']);
      // $redirect_url = _webform_filter_values($redirect_url, $node, NULL, NULL, FALSE, TRUE);
      // $output[] = ctools_ajax_command_redirect($redirect_url);
    }
    else {
      // We are on a multistep form step.
      $form_state['executed'] = array();
      $output = ctools_modal_form_wrapper('webform_client_form_' . $node->nid, $form_state);
    }
  }

  print ajax_render($output);
}

/**
 * Closes modal windows.
 */
function modal_forms_dismiss($js = NULL) {
  if (!$js) {
    // we should never be here out of ajax context
    return MENU_NOT_FOUND;
  }

  ctools_include('modal');
  ctools_include('ajax');

  $output = array(ctools_modal_command_dismiss());

  print ajax_render($output);
}

