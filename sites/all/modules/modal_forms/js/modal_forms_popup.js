/**
* Provide the HTML to create the modal dialog.
*/
Drupal.theme.prototype.ModalFormsPopup = function () {
  var html = '';

  html += '<div id="ctools-modal" class="popups-box">';
  html += '  <div class="ctools-modal-content modal-forms-modal-content lightBoxInnerPages">';
  html += '    <div class="popups-container module signUp uploadImage">';
  html += '        <span class="popups-close close btnClose">' + Drupal.CTools.Modal.currentSettings.closeText + '</span>';
  html += '      <div class="modal-scroll tabContentWrapper"><h2 id="modal-title" class="modal-title"></h2><div id="modal-content" class="modal-content popups-body"></div></div>';
  html += '    </div>';
  html += '  </div>';
  html += '</div>';

  return html;
}
