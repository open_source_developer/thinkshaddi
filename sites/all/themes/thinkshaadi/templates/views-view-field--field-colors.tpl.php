<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
$count = count($row->field_field_colors);

?>
<?php if($output): ?>
	<ul class="colors">
	<?php for($i=0;$i<$count;$i++){ ?>
		<?php
			$term = get_hex_color($row->field_field_colors[$i]['raw']['tid']);
		?>
		<?php if(isset($term['name']) || isset($term['color'])): ?>
			<?php
			  $title = isset($term['name']) ? $term['name'] : '';
			  $class = isset($term['name']) ? strtolower(str_replace(' ','_', $term['name'])) : '';
			  $colorStyle = isset($term['color']) ? "style='background-color:".$term['color']."'" : '';
			  $url = isset($term['path'] ) ? $term['path'] : "#" ;
			?>
			<li>
				<a class="<?php echo $class; ?>" title="<?php echo $title; ?>" <?php echo $colorStyle; ?>  href="<?php echo  $url; ?>"><?php echo $title; ?></a>
			</li>
		<?php endif; ?>
	<?php } ?>
	</ul>
<?php endif; ?>
