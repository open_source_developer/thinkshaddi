<?php

/**
 * @file
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 *
 * @ingroup themeable
 */
global $user;
//dsm($user);
$userFlag = 0;
if($user->uid == $userId){
	$userFlag = 1;
}
$profile = profile2_load_by_user($userId,'vendor_profile');
$otherInfo = profile2_load_by_user($userId,'vendor_other_info');

$bundles = vendor_board( $userId);
$bundleCount = 0;
if(is_array($bundles) && count($bundles) > 0){
  $bundleCount = count($bundles);
}
//$faq = get_faq($userId);
//dsm($faq);
global $base_url;
$name = "";
$fbUrl = "";
$twitterUrl = "";
$address = "";
$profileTab = "";
$worldWide = "";
$userPic = $base_url."/".drupal_get_path('theme', 'thinkshaadi')."/assets/img/vendor_img.jpg";
//dsm($account->picture);
$featured = 0;
if(isset($otherInfo->field_featured_vendor['und'][0]['value']) && !empty($otherInfo->field_featured_vendor['und'][0]['value'])){
  $featured = 1;	  

}
if(isset($account->picture->uri) && !empty($account->picture->uri)){
	$userPic =  image_style_url('vendor-image-style', $account->picture->uri);//.file_create_url($account->picture->uri);
}
if(isset($account->field_first_name['und'][0]['value'])){
  $name = $account->field_first_name['und'][0]['value'];
}
if(isset($account->field_last_name['und'][0]['value'])){
  $name .= " ".$account->field_last_name['und'][0]['value'];	 
}
if(isset($profile->field_facebook_page['und'][0]['url'])){
  $fbUrl = $profile->field_facebook_page['und'][0]['url'];
}
if(isset($profile->field_facebook_page['und'][0]['url'])){
  $twitterUrl = $profile->field_twitter_page['und'][0]['url'];
}

/*for($i=0;$i<5;$i++){
	$address = "";
	if(isset($profile->field_primary_location['und'][$i]['value'])){
	  $address = $profile->field_primary_location['und'][$i]['value'];
	}
	if(isset($profile->field_location_city['und'][$i]['tid'])){
	  if(!empty($address)){
	  	$address .=", ";
	  }	
	  $address .= $profile->field_location_city['und'][$i]['taxonomy_term']->name;
	}
	if(isset($profile->field_pincode['und'][$i]['value'])){
	  if(!empty($address)){
	  	$address .=" ";
	  }	
	  $address .= $profile->field_pincode['und'][$i]['value'];
	}
	if(isset($profile->field_location_country['und'][$i]['tid'])){
	  if(!empty($address)){
	  	$address .=", ";
	  }	
	  $address .=  $profile->field_location_country['und'][$i]['taxonomy_term']->name;
	}
	if(!empty($address)){
		$addrArray[] = $address;
	}
}*/
if(isset($otherInfo->field_world_wide_service['und'][0]['value'])){
  $worldWide = 	$otherInfo->field_world_wide_service['und'][0]['value'];
}
$i=0;
$address = "";
if(isset($profile->field_primary_location['und'][$i]['value'])){
  $address = $profile->field_primary_location['und'][$i]['value'];
}
if(isset($profile->field_location_city['und'][$i]['tid'])){
  if(!empty($address)){
  	$address .=", ";
  }	
  $address .= $profile->field_location_city['und'][$i]['taxonomy_term']->name;
}
if(isset($profile->field_pincode['und'][$i]['value'])){
  if(!empty($address)){
  	$address .=" ";
  }	
  $address .= $profile->field_pincode['und'][$i]['value'];
}
if(isset($profile->field_location_country['und'][$i]['tid'])){
  if(!empty($address)){
  	$address .=", ";
  }	
  $address .=  $profile->field_location_country['und'][$i]['taxonomy_term']->name;
}
if(!empty($address)){
	$addrArray[] = $address;
}
//dsm($addrArray);
$countryCount = count($profile->field_country['und']);
$country = '';
for($i=0;$i<$countryCount;$i++){
  if($country <> ""){
  	$country .=", ";
  }	
  $country .= $profile->field_country['und'][$i]['taxonomy_term']->name;
}
$stateCount = count($profile->field_state['und']);
$state = '';
for($i=0;$i<$stateCount;$i++){
  if($state <> ""){
  	$state .=", ";
  }	
  $state .= $profile->field_state['und'][$i]['taxonomy_term']->name;
}
$locCount = count($profile->field_service_locations['und']);
$city = '';
for($i=0;$i<$locCount;$i++){
  if($city <> ""){
  	$city .=", ";
  }	
  $city .= $profile->field_service_locations['und'][$i]['taxonomy_term']->name;
}

$catCount = count($otherInfo->field_category['und']);
$cat = '';
for($i=0;$i<$catCount;$i++){
  if($cat <> ""){
  	$cat .=", ";
  }	
  $cat .= $otherInfo->field_category['und'][$i]['taxonomy_term']->name;
}

$phoneNo = "";
if(isset($otherInfo->field_phone_no['und'][0]['value'])){
	$phoneNo = $otherInfo->field_phone_no['und'][0]['value'];
}

$email = $account->mail;
if(isset($otherInfo->field_profile_info['und'][0]['value'])){
  $profileTab = $otherInfo->field_profile_info['und'][0]['value'];
}
?>
<div class="profile module vendorDetails"<?php print $attributes; ?>>
  <div class="vendorProfile cols">
  	<div class="col1 summary">
  	  <div class="user-pic">
  	    <img src="<?php echo $userPic ; ?>" alt="<?php echo $name; ?>" />
  	  </div>
  	  <ul>
		<li class="vLike"><a href="#">Like</a><span class="count">0</span></li>
		<?php if($fbUrl) : ?>
			<li class="vFBook"><a href="<?php echo $fbUrl; ?>" target="_blank">Facebook</a></li>
		<?php endif; ?>
		<?php if($twitterUrl) : ?>
			<li class="vTwitter"><a href="<?php echo $twitterUrl; ?>" target="_blank">Twitter</a></li>
		<?php endif; ?>
		<?php if($email) : ?>
			<li class="vEmail"><a href="mailto:<?php echo $email; ?>">Email</a></li>
		<?php endif; ?>
		
	  </ul>
	  <div>
	  		<?php if(user_access('administer users')): ?>
		  		<?php if($featured == 1): ?>
		  		  <a href="<?php echo $base_url; ?>/unfeatured/nojs/vendor/<?php echo $userId; ?>">Unfeature Vendor</a>	
		  		<?php else: ?> 
		  		  <a href="<?php echo $base_url; ?>/featured/nojs/vendor/<?php echo $userId; ?>">Feature Vendor</a>		
		  		<?php endif; ?>
	  		<?php endif; ?>
	  </div>
  	</div>
  	<div class="col2 details">
  		<div class="detailsDiv">
			<h2 class="username"><?php echo $name; ?></h2>
			<!--strong>Fashion Designer</strong-->
			<p><span class="address">

				<?php if(count($addrArray) > 0): ?>
					<span>
				<?php endif; ?>
				<?php echo implode('</span><span>', $addrArray); ?>
				<?php if(count($addrArray) > 0): ?>
					</span>
				<?php endif; ?>
			</span><a href="#">Map »</a></p>
			<ul class="vendorInfo">
				<?php if(empty($worldWide) || $worldWide == 'N'): ?>
					<li class="vLocations"><span class="countryList"><?php echo $country; ?></span><a href="#">more »</a></li>
					<?php if(!empty($state)) : ?>
						<li class="vLocations"><span class="stateList"><?php echo $state; ?></span><a href="#">more »</a></li>
					<?php endif; ?>
					<?php if(!empty($city)) : ?>
						<li class="vLocations"><span class="locationList"><?php echo $city; ?></span><a href="#">more »</a></li>
					<?php endif; ?>
				<?php elseif($worldWide == 'Y'): ?>
					<li class="vLocations">Worldwide Service</li>
				<?php endif; ?>
				<li class="vCategory"><span class="catList"><?php echo $cat; ?></span> <a href="#">more »</a></li>
				<li class="vPhone"><?php echo $phoneNo; ?></li>
				<li class="vWebsite"><a href="<?php echo $website; ?>" target="_blank">Visit my website</a></li>
				<li class="vMoreInfo"><a href="#">More Details</a></li>
			</ul>
		</div>
		<?php if($userFlag): ?>
	  	<a href="#" class="userEditHref">Edit</a>
	  	<div class="profileFormSucess" style="display:none"></div>
		<div class="profileFormDiv" >
			<?php print render($account->form['profile_form']); ?>
		</div>
	  <?php endif; ?>
	</div>
  </div>
  <div class="vendorTabs fnTabs">
  	<ul class="tabs">
		<li class="active"><a href="#">Boards</a></li>
		<li><a href="#">Profile</a></li>
		<!--<li><a href="#">Reviews<span>(50)</span></a></li>-->
		<li><a href="<?php echo $base_url; ?>/faq/nojs/<?php echo $userId; ?>" class="use-ajax">FAQ's</a></li>
		<li><a href="<?php echo $base_url; ?>/map-tab/nojs/<?php echo $userId; ?>" class="use-ajax">Map</a></li>
	</ul>
	<ul class="tabContent">
		<li class="boardContent bundleResults">
			 <ul>
		 		<?php if($userFlag): ?>
			 	<li class="addNewBundle">
					<a href="<?php echo $base_url."/modal_forms/nojs/node/add/bundles" ?>" class="ctools-use-modal ctools-modal-modal-popup-large">Create new Board</a>
				</li>
				<?php endif; ?>
			 	<?php for($j=0;$j<$bundleCount;$j++){ ?>
			 	  <li rel="<?php echo $bundles[$j]['bundleurl'];  ?>">	
			 	   <div class="thumbCollection">
			 	   	<?php $imgCount = count($bundles[$j]['bundle']); ?>
			 	   	<?php for($f=0;$f<$imgCount;$f++){ ?>
			 	   		<?php  
			 	   			$imageClass = "";
			 	   			$imgPath =   $bundles[$j]['bundle'][$f]['imagePath'];
			 	   			
			 	   		?>
			 	   		<?php if($f==0) : ?>
			 	   			<div class ="bigThumb" >
			 	   		<?php endif; ?>
						<img src="<?php echo $imgPath; ?>" alt="" />
						<?php if($f==0) : ?>
			 	   			</div >
			 	   		<?php endif; ?>
					<?php } ?>
					</div>
						<h3><a href="<?php echo $bundles[$j]['bundleurl'];  ?>"><?php echo $bundles[$j]['title']; ?></a></h3>
						<h2><a href="<?php echo $bundles[$j]['vendorUrl'];  ?>"><?php echo $name; ?></a></h2>
						<hr/>
						<ul class="hoverState userLikes">
							<li><a title="10" href="#" class="btnLike">Like it</a><span class="count">0</span></li>
							<li><a title="50" href="#" class="btnFav">favorite it</a><span class="count">0</span></li>
						</ul>
				   </li>
			 	<?php } ?>
			 </ul>
		</li>
		<li class="profileContent">
			<div class="profileText">
				<?php echo $profileTab; ?>
			</div>
			<?php if($userFlag): ?>
				<div><a href="#" class ="btn_small_pink"><span>Edit</span></a></div>
			<?php endif; ?>
			<div class="profileError" style="display:none"></div>	
			<div class="profileForm">
				<?php print render($account->form['profile']); ?>
			</div>
		</li>
		<li class="faqContent">
				<div class="faqList">
				</div>
				<div class="divForm">
				</div>
		</li>
		<li class="mapContent">

		</li>
	</ul>
  </div>
</div>

