<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */

global $base_url;
$type = $row->_field_data['nid']['entity']->type;
$nid = $row->nid;
$dealNode = query_data_get_deal($nid,$type,1);
?>
<?php if(isset($dealNode->field_deal_description['und'][0]['value']) && !empty($dealNode->field_deal_description['und'][0]['value'])) : ?>
	<a href="<?php echo $base_url; ?>/modal_forms/nojs/vendor-deals-view/<?php echo $row->nid; ?>" class="ctools-use-modal ctools-modal-modal-popup-large" title="View Deal">View Deal</a>
<?php endif; ?>