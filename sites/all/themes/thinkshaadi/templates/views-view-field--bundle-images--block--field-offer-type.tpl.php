<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */


$type = $row->_field_data['nid']['entity']->type;
$nid = $row->nid;
$optionNode = query_data_get_option($nid,$type,1);
$symbol = "";

$price = $optionNode->field_price['und'][0]['value'];
$currencyTid = $optionNode->field_currency['und'][0]['tid'];
$currencyTax = get_currency_symbol($currencyTid);

if(isset($currencyTax['symbol'])){
	$symbol = $currencyTax['symbol'];
}
?>
<?php if(isset($optionNode->field_offer_type['und'][0]['value']) && isset($optionNode->field_offer_link['und'][0]['url'])) : ?>
	<span class="category price">Price</span><span class="curencySymbol"><?php echo $symbol; ?></span><span class="priceTag"><?php echo $price; ?></span><a href="<?php echo $optionNode->field_offer_link['und'][0]['url']; ?>" title="<?php echo ucwords($optionNode->field_offer_type['und'][0]['value']); ?>" class="offerLink <?php echo $optionNode->field_offer_type['und'][0]['value']; ?>" target="_blank"><?php print ucwords($optionNode->field_offer_type['und'][0]['value']); ?></a>
<?php endif; ?>