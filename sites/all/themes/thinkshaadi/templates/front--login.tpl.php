<?php
/**
 * @file
 * Zen theme's implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $secondary_menu_heading: The title of the menu used by the secondary links.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['navigation']: Items for the navigation region, below the main menu (if any).
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['footer']: Items for the footer region.
 * - $page['bottom']: Items to appear at the bottom of the page below the footer.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see zen_preprocess_page()
 * @see template_process()
 */
global $base_url;
global $user;  
$nodeType = "";
if(isset($node->type) && !empty($node->type)){
  $nodeType = $node->type;
  
}
$colors = getColors();
?>
<div class="module topHeader">
    <div class="wrapper">
        <?php print render($page['header_top']); ?>
    </div>
</div>
<div id="nav" class="module globalHeader">
	<div class="wrapper"> 
		<?php if ($logo): ?>
			<div class="logoWrapper">
			  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
			  <?php if ($site_slogan): ?>  
			  <h1><?php print $site_slogan; ?></h1>
			  <?php endif; ?>
			</div>	
		<?php endif; ?>
	   <?php if ($main_menu): ?>
        <div id="main-menu" role="navigation">
          <?php
          // This code snippet is hard to modify. We recommend turning off the
          // "Main menu" on your sub-theme's settings form, deleting this PHP
          // code block, and, instead, using the "Menu block" module.
          // @see http://drupal.org/project/menu_block
          print theme('links__system_main_menu', array(
            'links' => $main_menu,
            'attributes' => array(
              'class' => array('links', 'inline', 'clearfix','primaryNavigation','tabs'),
            ),
          )); ?>
        </div>
      <?php endif; ?>
      <?php include_once 'vendor-cta.tpl.php'; ?>
      <?php if($page['header_bottom']) : ?>
        <div><?php print render($page['header_bottom']); ?></div>
      <?php endif; ?>
	</div>
</div>
<div class="module content">
  <?php include_once 'color-pallete.tpl.php'; ?>
  <?php print $messages; ?>
  <div class="wrapper infiniteData">
    <div id="content" class="categoryWrapper hasSubMenu">
        <ul>
            <li class="active">
                <a href="#" class="catFeatured">Featured</a>
                <span></span>
              <div class="subMenu">
                <ul>
                  <li class="active">
                    <a href="#"  class="catFeatured" >Images</a>
                  </li>
                  <li>
                    <a href="#" class="catBundles">Boards</a>
                  </li>
                </ul>
              </div>
            </li>
            <li>
                <a href="#" class="catPopular">Popular</a>
                <span></span>
            </li>
            <li>
                <a href="#" class="catCategory">Categories</a>
            </li>
        </ul>
    </div>
    <div class="categoryResults">
        <ul class="thumbList">
          <!--Thumbnails will generate here with AJAX -->
          
        </ul>
      </div>
    <div class="loadMoreAjaxLoader">
      <img src="sites/all/themes/thinkshaadi/assets/img/loader.gif" />
    </div>
    <ul class="aboutTopLinkWrapper">
          <li><a href="#" class="linkAboutUs"><span class="assist">About</span></a></li>
          <li><a href="#" class="linkBackToTop"><span class="assist">Top</span></a></li>
      </ul>
  </div>
</div>
<div id="footer" class="module footer" >
    <div class="wrapper">
        <ul class="socialMedia">
            <li><a href="https://www.facebook.com/ThinkShaadi" target="_blank">facebook</a></li>
            <li><a href="https://twitter.com/ThinkShaadi" class="twitter" target="_blank">twitter</a></li>
            <li><a target="_blank" href="https://plus.google.com/100911382509592635947" class="googlePlus">google+</a></li>
            <!--<li><a href="#" class="pinterest">pinterest</a></li>-->
        </ul>
        <ul class="footerLinks">
            <li><a href="#">Contact</a></li>
            <li><a href="#">Terms of Use</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Legal Notice</a></li>
        </ul>

        <!--p>Copyright © &amp; Design by Company Name</p-->
        <p>Copyright © &amp; Thinkshaadi 2013.</p>
        <a href="#" class="linkBackToTop">Back to top</a>
        <a href="#" class="btnClose">Close</a>
    </div>
</div>


<?php print render($page['bottom']); ?>
