<?php


?>
<div class="betaWrapper">
	<div class="topHeader">
		<div class="wrapper">
			<?php
			  $block = module_invoke('menu', 'block_view', 'menu-top-beta-menu');
			  print drupal_render($block['content']);
			?>
			<div class="social">
			    <ul class="socialMedia">
                        <li><a target="_blank" href="https://www.facebook.com/ThinkShaadi">facebook</a></li>
                        <li><a target="_blank" class="twitter" href="https://twitter.com/ThinkShaadi">twitter</a></li>
                        <li><a target="_blank" href="https://plus.google.com/100911382509592635947" class="googlePlus">google+</a></li>
                        <!--<li><a href="#" class="pinterest">pinterest</a></li>-->
                    </ul>
			</div>
		</div>
	</div>
	<?php if ($logo): ?>
		<div class="infoBlock">
		  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print drupal_get_path('theme', 'thinkshaadi'); ?>/assets/img/logo_beta.png" alt="<?php print t('Home'); ?>" /></a>
		  <?php if ($site_slogan): ?>  
		  <!--h1><?php print $site_slogan; ?></h1-->
		  <?php endif; ?>
		</div>	
	<?php endif; ?>
	<div class="invitationPanel">
		<div class="selection">
			<img src="sites/all/themes/thinkshaadi/assets/img/request_an_invite.png">
			<ul>
				<li><a class="btn_pink btn_user <?php if($form == "user"){ echo "active"; } ?>" href="<?php echo $base_url."/user-beta-form" ?>" ><span>As a User</span></a></li>
				<li><a class="btn_pink btn_vendor <?php if($form == "vendor"){ echo "active"; } ?>" href="<?php echo $base_url."/vendor-beta-form" ?>" ><span>As a Vendor</span></a></li>
			</ul>
		</div>
		<div class="errorDiv"></div>
		<div class="succDiv"></div>
		<?php print $messages; ?>
		<div class="userSignup" >
			<h2>User Signup</h2>
			<?php
			  $block = module_invoke('beta_form_user', 'block_view', 'info');
			  print drupal_render($block['content']);
			?>
		</div>
		<div class="vendorSignup" >
			<h2>Vendor Signup</h2>
			<?php
			  $block = module_invoke('beta_form_vendor', 'block_view', 'info');
			  print drupal_render($block['content']);
			?>
		</div>
	</div>
	
</div>