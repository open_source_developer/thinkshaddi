<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
global $base_url;
$link =$base_url."/";
if($view->field['field_associated_vendors']->view->result[0]->users_node_uid){
	$link .= drupal_get_path_alias('user/' . $view->field['field_associated_vendors']->view->result[0]->users_node_uid);
}

?>
<ul>
<?php
$vendorCount = count($view->field['field_associated_vendors']->view->result[0]->field_field_associated_vendors);
$fName = $view->field['field_first_name']->view->result[0]->field_field_first_name[0]['raw']['value'];
$lName = $view->field['field_last_name']->view->result[0]->field_field_last_name[0]['raw']['value'];
?>
<li><a href="<?php echo $link ?>"><?php echo $fName." ".$lName; ?></a></li>
<?php
for($i=0;$i<$vendorCount;$i++){
	$vendorId = $view->field['field_associated_vendors']->view->result[0]->field_field_associated_vendors[$i]['raw']['target_id'];
	$user = user_load($vendorId);
	$link = $base_url."/".drupal_get_path_alias('user/' .$vendorId );
	echo "<li><a href='".$link."'>".$user->field_first_name['und'][0]['value']." ".$user->field_last_name['und'][0]['value']."</a></li>";
}

?>
</ul>
