<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */


global $base_url;
global $user;
?>
<?php if($output && ($row->users_node_uid == $user->uid || array_key_exists(3,$user->roles))) : ?>
	<div><a class="ctools-use-modal ctools-modal-modal-popup-large btn_small_pink" href="<?php echo $base_url; ?>/modal_forms/nojs/node/<?php echo $row->nid; ?>/edit" title="Edit"><span class="edit">Edit</span></a></div>
<?php endif; ?>
<?php if($row->users_node_uid == $user->uid) : ?>
	<div><a class="ctools-use-modal ctools-modal-modal-popup-large btn_small_pink" href="<?php echo $base_url; ?>/modal_forms/nojs/vendor-options/<?php echo $row->nid; ?>" title="Add Option"><span class="option">Add Option</span></a></div>
	<div><a class="ctools-use-modal ctools-modal-modal-popup-large btn_small_pink" href="<?php echo $base_url; ?>/modal_forms/nojs/vendor-deals/<?php echo $row->nid; ?>" title="Add Deal"><span class="deal">Add deal</span></a></div>
<?php endif; ?>