<?php if(isset($colors) && is_array($colors) && count($colors) > 0): ?>
  <div class="colorOptions">
  <a href="#" class="linkToggleViewColor">Close</a>
    <div class="heading">
      <span><h2>Filter Images by Color</h2></span>
    </div>
    <ul class="colors">
        <?php 
          foreach($colors as $key => $value){
            ?>
              <li>
                <a href="<?php echo $value['path']; ?>" <?php if(!empty($value['hex'])) { echo "style='background-color:".$value['hex']."'"; } ?> title="<?php echo $value['name']; ?>" class="<?php echo $value['class']; ?>"><?php echo $value['name']; ?></a>
              </li>
            <?php
          }
        ?>
        
    </ul>
</div>
<?php endif; ?>