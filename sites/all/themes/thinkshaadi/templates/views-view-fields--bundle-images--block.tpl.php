<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

$fieldsPresent = array_keys($fields);

 $groupFields = array("field_offer_type","field_deal_description","field_photographer","field_associated_vendors","field_image_tags","field_category","field_colors");
 $groupStart = "";
 $groupEnd = "";
 for($i=0;$i<count($groupFields);$i++){
	if(in_array($groupFields[$i],$fieldsPresent)){
		if(empty($groupStart)){
      if($groupFields[$i] == "field_photographer" && empty($row->field_field_photographer)){
         continue;
      }
			$groupStart = $groupFields[$i];
		}else{
			$groupEnd = $groupFields[$i];
		}
	}
 }
 if(!empty($groupStart) && empty($groupEnd)){
	$groupEnd = $groupStart;
 }
?>
<?php foreach ($fields as $id => $field): ?>
  <?php if($id=="field_photographer" && empty($row->field_field_photographer)): ?>
    <?php  continue; ?>
  <?php endif; ?>
  <?php if (!empty($groupStart) && !empty($groupEnd) && $id == $groupStart): ?>
    <ul class="grouping_field collectionDetails">
  <?php endif; ?>	
  <?php if (!empty($field->separator)): ?>
    <?php print $field->separator; ?>
  <?php endif; ?>
  <?php if (in_array($id,$groupFields)): ?>
  	<li>
  <?php else : ?>		
  	<?php print $field->wrapper_prefix; ?>
  <?php endif; ?>
    <?php print $field->label_html; ?>
    <?php print $field->content; ?>
  
  <?php if (in_array($id,$groupFields)): ?>
  	</li>
  <?php else : ?>		
  	<?php print $field->wrapper_suffix; ?>
  <?php endif; ?>
  <?php if (!empty($groupStart) && !empty($groupEnd) && $id == $groupEnd): ?>
    </ul>
  <?php endif; ?>
  <?php if($id == "field_logo" || $id == "title"): ?> 	
    <hr/>
  <?php endif; ?>
<?php endforeach; ?>