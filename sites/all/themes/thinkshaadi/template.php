<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * A QUICK OVERVIEW OF DRUPAL THEMING
 *
 *   The default HTML for all of Drupal's markup is specified by its modules.
 *   For example, the comment.module provides the default HTML markup and CSS
 *   styling that is wrapped around each comment. Fortunately, each piece of
 *   markup can optionally be overridden by the theme.
 *
 *   Drupal deals with each chunk of content using a "theme hook". The raw
 *   content is placed in PHP variables and passed through the theme hook, which
 *   can either be a template file (which you should already be familiary with)
 *   or a theme function. For example, the "comment" theme hook is implemented
 *   with a comment.tpl.php template file, but the "breadcrumb" theme hooks is
 *   implemented with a theme_breadcrumb() theme function. Regardless if the
 *   theme hook uses a template file or theme function, the template or function
 *   does the same kind of work; it takes the PHP variables passed to it and
 *   wraps the raw content with the desired HTML markup.
 *
 *   Most theme hooks are implemented with template files. Theme hooks that use
 *   theme functions do so for performance reasons - theme_field() is faster
 *   than a field.tpl.php - or for legacy reasons - theme_breadcrumb() has "been
 *   that way forever."
 *
 *   The variables used by theme functions or template files come from a handful
 *   of sources:
 *   - the contents of other theme hooks that have already been rendered into
 *     HTML. For example, the HTML from theme_breadcrumb() is put into the
 *     $breadcrumb variable of the page.tpl.php template file.
 *   - raw data provided directly by a module (often pulled from a database)
 *   - a "render element" provided directly by a module. A render element is a
 *     nested PHP array which contains both content and meta data with hints on
 *     how the content should be rendered. If a variable in a template file is a
 *     render element, it needs to be rendered with the render() function and
 *     then printed using:
 *       <?php print render($variable); ?>
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. With this file you can do three things:
 *   - Modify any theme hooks variables or add your own variables, using
 *     preprocess or process functions.
 *   - Override any theme function. That is, replace a module's default theme
 *     function with one you write.
 *   - Call hook_*_alter() functions which allow you to alter various parts of
 *     Drupal's internals, including the render elements in forms. The most
 *     useful of which include hook_form_alter(), hook_form_FORM_ID_alter(),
 *     and hook_page_alter(). See api.drupal.org for more information about
 *     _alter functions.
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   If a theme hook uses a theme function, Drupal will use the default theme
 *   function unless your theme overrides it. To override a theme function, you
 *   have to first find the theme function that generates the output. (The
 *   api.drupal.org website is a good place to find which file contains which
 *   function.) Then you can copy the original function in its entirety and
 *   paste it in this template.php file, changing the prefix from theme_ to
 *   STARTERKIT_. For example:
 *
 *     original, found in modules/field/field.module: theme_field()
 *     theme override, found in template.php: STARTERKIT_field()
 *
 *   where STARTERKIT is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_field() function.
 *
 *   Note that base themes can also override theme functions. And those
 *   overrides will be used by sub-themes unless the sub-theme chooses to
 *   override again.
 *
 *   Zen core only overrides one theme function. If you wish to override it, you
 *   should first look at how Zen core implements this function:
 *     theme_breadcrumbs()      in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold vari q ous pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called theme hook suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node--forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and theme hook suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440 and http://drupal.org/node/1089656
 */


/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_maintenance_page(&$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  STARTERKIT_preprocess_html($variables, $hook);
  STARTERKIT_preprocess_page($variables, $hook);
}
// */

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_html(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // The body tag's classes are controlled by the $classes_array variable. To
  // remove a class from $classes_array, use array_diff().
  //$variables['classes_array'] = array_diff($variables['classes_array'], array('class-to-remove'));
}
// */

function  thinkshaadi_preprocess_html(&$vars) {
  $vars['classes_array'][] = 'front-theme';
   if(drupal_is_front_page()){
     $vars['head_title'] = implode(' | ', array("Home", variable_get('site_name'))); 
   }
}
function thinkshaadi_form_element($variables){
  // This is also used in the installer, pre-database setup.
 $element = &$variables['element'];

  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array('form-item');
  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  if($element['#parents'][0] == "field_colors"){

    if($element['#type'] == "checkbox"){
       $taxonomy = taxonomy_term_load($element['#return_value']);
       if(isset($taxonomy->field_hex_value['und'][0]['value']) && !empty($taxonomy->field_hex_value['und'][0]['value'])){
         $attributes['style'] = array("background-color: ".$taxonomy->field_hex_value['und'][0]['value'].";");   
       }else{
          $attributes['class'][] = strtolower($element['name']);
       } 
       
      
    }
 }
 
  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  if(isset($element['#field_name']) && $element['#field_name'] == "field_colors"){
       $element['#attributes']['class'][] = "colors";
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  if (!empty($element['#description'])) {
    $output .= '<div class="description">' . $element['#description'] . "</div>\n";
  }
  
    
  $output .= "</div>\n";

  return $output;
}

function thinkshaadi_checkbox($variables) {
  $element = $variables['element'];
 
  $t = get_t();
  $element['#attributes']['type'] = 'checkbox';
  element_set_attributes($element, array('id', 'name','#return_value' => 'value'));
   if($element['#parents'][0] == "field_colors"){
       $element['#attributes']['class'][] = strtolower($element['#title']);  

   }   
  // Unchecked checkbox has #value of integer 0.
  if (!empty($element['#checked'])) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form-checkbox'));

  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */

function thinkshaadi_preprocess_page(&$variables, $hook) {
     //dsm(drupal_get_form('user_register_form'));
     //drupal_add_library('system', 'ui');
     global $user;
     ctools_include('ajax');
     ctools_include('modal');
     ctools_modal_add_js();
      global $base_url;
     drupal_add_js('var baseUrl ="'.$base_url.'/drupal_services/";', 'inline');
     if($variables['node']->nid == 40){
      
       drupal_add_css(drupal_get_path('theme', 'thinkshaadi') . '/assets/css/lib/royalslider.css');
       drupal_add_css(drupal_get_path('theme', 'thinkshaadi') . '/assets/css/modules/m_05_00_hero_carousal.css');
     }
     // Check user is logged in for beta phase page
    
     if($user->uid){
       drupal_add_css(drupal_get_path('theme', 'thinkshaadi') . '/assets/css/common/base.css');
       drupal_add_css(drupal_get_path('theme', 'thinkshaadi') . '/assets/css/common/fonts.css');
       drupal_add_css(drupal_get_path('theme', 'thinkshaadi') . '/assets/css/common/grid.css'); 
       drupal_add_css(drupal_get_path('theme', 'thinkshaadi') . '/assets/css/modules/imports_1.css');
       if(drupal_is_front_page() || (isset($variables['node']) && $variables['node']->type == "bundles")){
       //dsm($variables['node']);
           if(drupal_is_front_page() || (isset($variables['node']) && $variables['node']->type == "bundles")){
              drupal_add_js("var params=''",'inline');
              if((isset($variables['node']) && $variables['node']->type == "bundles")){
                drupal_add_js('params = "bundle='.$variables['node']->nid.'";', 'inline');
                //drupal_add_js('var baseUrl ="'.$base_url.'/drupal_services/images/?bundle='.$variables['node']->nid.'";', 'inline');
              }
                
              

              drupal_add_js(drupal_get_path('theme', 'thinkshaadi') .'/assets/js/lib/arrangeBoxes.js', 'file');
              drupal_add_js(drupal_get_path('theme', 'thinkshaadi') .'/assets/js/modules/m_03_00_thumb_view.js', 'file');
              
              drupal_add_js(drupal_get_path('theme', 'thinkshaadi') .'/assets/js/modules/m_09_00_category_listing.js', 'file');

           }
       }
       if($variables['theme_hook_suggestions'][0] == "page__user"){
         drupal_add_js('https://maps.googleapis.com/maps/api/js?key=AIzaSyCEY_7C_QtrVRSrHkctep_AvaecNCGjo7g&sensor=false', 'external');
         drupal_add_js(drupal_get_path('theme', 'thinkshaadi') .'/assets/js/modules/google_map.js', 'file');

       }
       if($variables['node']->type == "bundle_images" || $variables['node']->type == "board_videos"){
         drupal_add_js('http://cdn.webrupee.com/js', 'external');
       }  
     }else{
       if(drupal_is_front_page()){
         drupal_add_css(drupal_get_path('theme', 'thinkshaadi') . '/assets/css/beta/base.css');
         drupal_add_css(drupal_get_path('theme', 'thinkshaadi') . '/assets/css/beta/fonts.css');
         drupal_add_css(drupal_get_path('theme', 'thinkshaadi') . '/assets/css/modules/imports_1.css');
         drupal_add_css(drupal_get_path('theme', 'thinkshaadi') . '/assets/css/beta/index.css');
       }else{
         drupal_add_css(drupal_get_path('theme', 'thinkshaadi') . '/assets/css/common/base.css');
         drupal_add_css(drupal_get_path('theme', 'thinkshaadi') . '/assets/css/common/fonts.css');
         drupal_add_css(drupal_get_path('theme', 'thinkshaadi') . '/assets/css/common/grid.css'); 
         drupal_add_css(drupal_get_path('theme', 'thinkshaadi') . '/assets/css/modules/imports_1.css');
       }
     }
     

     if (arg(0) == 'taxonomy' && arg(1) == "term") {
        unset($variables['page']['content']['system_main']['nodes']);
        unset($variables['page']['content']['system_main']['pager']);
       $variables['theme_hook_suggestions'][] = 'page__taxonomy';
     }
}

function thinkshaadi_preprocess_taxonomy_term(&$variables) {
  global $base_url;
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  $variables['term'] = $variables['elements']['#term'];
  $term = $variables['term'];

  $uri = entity_uri('taxonomy_term', $term);
  $variables['term_url'] = url($uri['path'], $uri['options']);
  $variables['term_name'] = check_plain($term->name);
  $variables['page'] = $variables['view_mode'] == 'full' && taxonomy_term_is_page($term);

  // Flatten the term object's member fields.
  $variables = array_merge((array) $term, $variables);

  // Helpful $content variable for templates.
  $variables['content'] = array();
  foreach (element_children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  // field_attach_preprocess() overwrites the $[field_name] variables with the
  // values of the field in the language that was selected for display, instead
  // of the raw values in $term->[field_name], which contain all values in all
  // languages.
  field_attach_preprocess('taxonomy_term', $term, $variables['content'], $variables);

  // Gather classes, and clean up name so there are no underscores.
  $vocabulary_name_css = str_replace('_', '-', $term->vocabulary_machine_name);
  $variables['classes_array'][] = 'vocabulary-' . $vocabulary_name_css;
  $taxonomyArray = array('color','category','bundle_image_tags');
  $variables['theme_hook_suggestions'][] = 'taxonomy_term__' . $term->vocabulary_machine_name;
  $variables['theme_hook_suggestions'][] = 'taxonomy_term__' . $term->tid;
  if(in_array($variables['vocabulary_machine_name'] , $taxonomyArray)){
       drupal_add_js('var baseUrl ="'.$base_url.'/drupal_services/";', 'inline');
       if($variables['vocabulary_machine_name'] == "category"){
          drupal_add_js('params = "category='.$variables['tid'].'";', 'inline');
       }elseif($variables['vocabulary_machine_name'] == "bundle_image_tags"){
          drupal_add_js('params = "tag='.$variables['tid'].'";', 'inline');
       }elseif($variables['vocabulary_machine_name'] == "color"){
          drupal_add_js('params = "color='.$variables['tid'].'";', 'inline');
       }
       drupal_add_js(drupal_get_path('theme', 'thinkshaadi') .'/assets/js/lib/arrangeBoxes.js', 'file');
       drupal_add_js(drupal_get_path('theme', 'thinkshaadi') .'/assets/js/modules/m_03_00_thumb_view.js', 'file');

  }
}

function thinkshaadi_preprocess_user_profile(&$variables) {


  $roles = $variables['elements']['#account']->roles;
 // dsm($roles);
  if(array_key_exists('4',$roles)){
    $variables['theme_hook_suggestions'][] = 'vendor__profile';
  }
 // dsm($variables);
}
// */


/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/*
function thinkshaadi(&$variables, $hook) {


  // Optionally, run node-type-specific preprocess functions, like
  // thinkshaadi_preprocess_node_page() or thinkshaadi_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
}
 */

/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function thinkshaadi_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the region templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function thinkshaadi_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--sidebar.tpl.php template for sidebars.
  //if (strpos($variables['region'], 'sidebar_') === 0) {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('region__sidebar'));
  //}
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function thinkshaadi_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  // $variables['classes_array'][] = 'count-' . $variables['block_id'];

  // By default, Zen will use the block--no-wrapper.tpl.php for the main
  // content. This optional bit of code undoes that:
  //if ($variables['block_html_id'] == 'block-system-main') {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('block__no_wrapper'));
  //}
}
// */


function getContentClass($leftSidebar,$rightSidebar){
  if($rightSidebar && $leftSidebar){
    return "threeColumn";
  }elseif($rightSidebar || $leftSidebar){
    return "twoColumn";
  }else{
    return "oneColumn";
  }
}

function getContentTypeClass($contentType){
  switch ($contentType) {
    case 'bundle_images':
      return "imageDetailsWrapper";
    break;
    case 'bundles':
      return "bundleDetailsContent";
    break;
    case 'board_videos':
      return "imageDetailsWrapper videoDetailsWrapper";
    break;
    default:
      return "";
      break;
  }
}
