/**
 * Created with JetBrains WebStorm.
 * User: Ashirwad
 * Date: 2/24/13
 * Time: 7:22 PM
 * To change this template use File | Settings | File Templates.
 */
/*jslint bitwise: true, eqeqeq: true, passfail: false, nomen: false, plusplus: false, undef: true, evil: true */
/*global window, document, $, jQuery, self, Modernizr */

var thinkShaadi = window.thinkShaadi || {};

var categoryUrl = baseUrl+'categories';


thinkShaadi.createCategoryThumbs = {
	//jsonURL: '../../../assets/js/json/images.json',
	getJson: function() {
		jQuery('.thumbList').remove();
		jQuery('.subMenu').fadeOut();
		jQuery('.hasSubMenu').animate({
			'paddingBottom': 30
		});
		jQuery('.categoryResults').append('<ul class="categoryList"></ul>');

		jQuery.ajax({
			type: "GET",
			url: categoryUrl,
			dataType: "json",
			beforeSend: function(){
				jQuery('.loadMoreAjaxLoader').show();
			},
			success: function(tdata){
				jQuery('.loadMoreAjaxLoader').hide();
				var data = new Object();
				data.result = tdata;
				data.next  = 0;
				var results = [];
				jQuery.each(data.result, function (key, val) {
					var imagesString = '<li id="category'+key+'" class="genericContainer1">' +
						'<a href="taxonomy/term/'+val.id+'"><img alt="'+val.name+'" src="'+val.image+'" width="146" /></a>' +
						'<h4>'+val.name+'</h4>' +
						'<span></span>'
						'</li>'
					results.push(imagesString);
				});
				jQuery(results).appendTo('.categoryList');
			},
			error: function (msg) {

			}
		});
	},
	init: function() {
		thinkShaadi.createCategoryThumbs.getJson();
        jQuery('.categoryListCompactView a.active').parent()
	}
}