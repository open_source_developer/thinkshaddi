/**
 * Created with JetBrains WebStorm.
 * User: Ashirwad
 * Date: 11/13/12
 * Time: 2:13 AM
 * To change this template use File | Settings | File Templates.
 */
/*jslint bitwise: true, eqeqeq: true, passfail: false, nomen: false, plusplus: false, undef: true, evil: true */
/*global window, document, $, jQuery, self, Modernizr */

// Define our root namespace if necessary
var thinkShaadi = window.thinkShaadi || {};

var url = baseUrl;
var next = 1;
var ajaxOn = 0;
var start = 0;
/**
 * @namespace Behaviour for the thumbnailView
 */

thinkShaadi.createThumbs = {
    //jsonURL: 'http://localhost/thinkshaddi/drupal_services/images/',
    getJson: function(jsonURL, type) {
        if(ajaxOn == 0){
            jQuery.ajax({
                type: "GET",
                url: jsonURL,
                dataType: "json",
                beforeSend: function(){
                    ajaxOn = 1;
                    jQuery('.loadMoreAjaxLoader').show();
                },
                success: function(data){
                    ajaxOn = 0;
                    start = start +1;
                    imgBundle = 0;

                    var results = [];
                    if(data.next == 1){
                        next = 1;
                        jQuery('.loadMoreAjaxLoader').text('').append('<img src="/sites/all/themes/thinkshaadi/assets/img/loader.gif" />');
                    }else{
                        next = 0;
                        url = "";
                        jQuery('.loadMoreAjaxLoader img').hide();
                        jQuery('.loadMoreAjaxLoader').show().append('No more results found.');
                    }

                    if(data.result.length > 0){
                        //console.log(data);
                        jQuery.each(data.result, function (key, val) {
                            if(type !== 'catBundles') {

                                jQuery('.categoryResults').removeClass('bundleResults');

                                //read categories
                                var len = val.category.length,
                                    categoryValues="";

                                for(var f=0;f<len;f++){
                                    categoryValues = categoryValues + '<a href="'+val.category[f].url+'">'+val.category[f].name+'</a>';
                                    if(len -1 != f){
                                        categoryValues = categoryValues + ',&nbsp;';
                                    }
                                }

                                var imagesString = '<li id="box'+key+'" class="box thumb" style="display:none">' +
                                    '<a href="'+val.url+'"><img alt="'+val.category+'" src="'+val.img+'" width="208" /></a>' +
                                    '<h3><a href="'+val.vendor.url+'">'+val.vendor.name+'</a></h3>' +
                                    '<h2>'+categoryValues +'</h2>'+
                                    '<div class="hoverState" rel="'+val.url+'">' +
                                    '<p>'+val.description+'</p>' +
                                    '<ul>' +
                                    '<li>' +
                                    '<a class="btnLike" href="#" title="'+val.likes+'">Like it</a>' +
                                    '</li>' +
                                    '<li class="last">' +
                                    '<a class="btnAdd" href="#" title="Add it">Add it</a>' +
                                    '</li>' +
                                    '</ul>' +
                                    '</div>' +
                                    '</li>';

                            }

                            else if(type === 'catBundles') {
                                jQuery('.categoryResults').addClass('bundleResults');
                                var len = val.bundle.length;
                                var imgBundle= "";
                                for(var f=0;f<len;f++){
                                    if(f==0){
                                        imgBundle = imgBundle + '<div class="bigThumb"><img alt="" src="'+val.bundle[f].imagePath+'" /></div>';
                                    }else{
                                        imgBundle = imgBundle +' <img alt="" src="'+val.bundle[f].imagePath+'" />';
                                    }
                                }
                                var imagesString = '<li rel="'+val.bundleurl+'">' +
                                    '<div class="thumbCollection">' +
                                    imgBundle +
                                    '</div>' +
                                    '<h3><a href="'+val.bundleurl+'">'+val.title+'</a></h3>' +
                                    '<h2><a href="'+val.vendorUrl+'">'+val.vendor+'</a></h2>' +
                                    '<hr>' +
                                    '<ul class="hoverStateBundle userLikes">' +
                                    '<li><a class="btnLike" href="#" title="10">Like it</a><span class="count">888</span></li>' +
                                    '<li><a class="btnFav" href="#" title="50">favorite it</a><span class="count">88</span></li>' +
                                    '</ul>' +
                                    '</li>';
                            }
                            results.push(imagesString);
                        });
                        if(start == 1){
                            jQuery('.thumbList').html('');

                        }
                        //console.log(results);
                        //jQuery(results).appendTo('.thumbList');
                        jQuery(results.join('')).appendTo('.thumbList');
                        thinkShaadi.arrange.init();
                        thinkShaadi.loadImageSequence.init();
                    }else{
                        url = '';
                        jQuery('.loadMoreAjaxLoader img').hide();
                        jQuery('.loadMoreAjaxLoader').show().append('No more results found.');
                    }
                    //jQuery('.loadMoreAjaxLoader').hide();
                },
                error: function (msg) {

                }
            });
        }

    },
    init: function() {
        var type;

        if(jQuery('.categoryWrapper .active .active a')){
            type = jQuery('.categoryWrapper .active .active a').attr('class');
        } else {
            type = jQuery('.categoryWrapper .active > a').attr('class');
        }


        url = baseUrl;
        if(jQuery('body').hasClass('front')){
            getUrl();
        }else{
            url = url+"images";
        }

        if(params != ""){
            url = url + "?"+params+"&page="+start;
        }else{
            url = url + "?page="+start;
        }

        if(url != ""){
            thinkShaadi.createThumbs.getJson(url, type);
        }
    }
};

thinkShaadi.arrange = {
    arrangeBoxes: function($container) {
        var thumbs = $container.find('.box');
        $container.imagesLoaded( function(){
            if(start == 1){
                jQuery('ul.thumbList li.box:hidden').show();
                $container.masonry({
                    itemSelector : thumbs,
                    columnWidth: 10,
                    isAnimated: false
                });
            }else{
                $container.masonry({
                    itemSelector : thumbs,
                    columnWidth: 10,
                    isAnimated: false
                });
                var $newElems = 	jQuery('ul.thumbList li.box:hidden');
                jQuery('ul.thumbList li:hidden').show();
                $container.masonry( 'appended', $newElems, true);
            }

        });
    },
    init: function(html) {
        thinkShaadi.arrange.arrangeBoxes(jQuery('.thumbList'),html);
    }
};

thinkShaadi.loadImageSequence = {
    imageSequence: function(obj,indx,delay){
        if(indx < jQuery(obj).length) {
            jQuery(obj).eq(indx).fadeIn(delay, function (){
                indx++;
                thinkShaadi.loadImageSequence.imageSequence(obj,indx,delay);
            });
        }
    },
    init: function(){
        var thumbs = jQuery(".categoryResults > ul > li");
        //thumbs.hide();
        //thinkShaadi.loadImageSequence.imageSequence(thumbs,0,500);
        thinkShaadi.thumnailView.thumbDetailSetup(jQuery('.categoryResults > ul > li'));
        thinkShaadi.lightBox.setup();
    }
};

thinkShaadi.thumnailView = {
    thumbSelector: jQuery('.categoryResults > ul > li'),
    thumbImage: jQuery('.categoryResults > ul > li').find('img'),

    thumbDetailSetup: function(selector) {
        selector.each(function() {
            thinkShaadi.thumnailView.bindEvents(jQuery(this), 300);
        })
    },
    bindEvents: function(thumb, duration) {
        thumb.bind({
            mouseenter: function() {
                var hoverState = jQuery(this).find('.hoverState');
                if(jQuery(this).height() > 100 ){
                    thinkShaadi.thumnailView.setHeightAndWidthToDetails(hoverState);
                } else {
                    return false;
                }
                thumb.find('.hoverState').stop(true, true).fadeIn(duration);
            }, mouseleave: function (){
                thumb.find('.hoverState').stop(true, true).fadeOut(duration);
            }
        });
        thumb.find('.hoverState').bind('click', function(){
            var detailImgUrl = thumb.find('.hoverState').attr('rel');
            window.location.href = detailImgUrl;
        });


        jQuery('.bundleResults > ul > li').live('click', function(){
           // jQuery()
            var detailImgUrl = jQuery(this).attr('rel');
            window.location.href = detailImgUrl;
        });
    },
    setHeightAndWidthToDetails: function(setMe) {
        var parent = setMe.parent('li').find('img'),
            height = parent.height()-20,
            width = parent.width()-20;
        setMe.css({
            height: height,
            width: width
        });
    },
    init: function() {
        if(this.thumbImage.length !== 0){
            this.thumbDetailSetup(this.thumbSelector);
        }
    }
};

var selected;
function getUrl(){

    if(selected === undefined) {
        selected = 'catFeatured'
    }

    jQuery('.categoryWrapper a').each(function() {
        jQuery(this).click(function(e){
            e.preventDefault();
            jQuery('.categoryResults .thumbList li').remove();
            jQuery('.footer').hide();
            jQuery('.categoryResults').removeClass('bundleResults');
            if(selected === undefined) {
                selected = 'catFeatured'
            } else {
                selected = jQuery(this).attr('class');
            }
        })
    });

    switch(selected){
        case 'catFeatured':
            url = url +"images";
            break;
        case 'catPopular':
        //url = url +"images";
        //break;
        case 'catBundles':
            url = baseUrl+'bundles';
            break;
        case 'catCategory':
            url = url+'categories';
            break;
        default:
            url = baseUrl;
            break;
    }
}

jQuery(window).scroll(function(){
    if(jQuery(window).scrollTop() == jQuery(document).height() - jQuery(window).height()){
        if(jQuery('.thumbList').length !== 0){
            if(url != ''){
                thinkShaadi.createThumbs.init();
            }
        } else {
            return false;
        }
    }
});