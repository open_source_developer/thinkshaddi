/**
 * Created with JetBrains WebStorm.
 * User: Ashirwad
 * Date: 11/13/12
 * Time: 2:13 AM
 * To change this template use File | Settings | File Templates.
 */
/*jslint bitwise: true, eqeqeq: true, passfail: false, nomen: false, plusplus: false, undef: true, evil: true */
/*global window, document, $, jQuery, self, Modernizr */

// Define our root namespace if necessary
var thinkShaadi = window.thinkShaadi || {};

/**
 * @namespace Behaviour for the thumbnailView
 */

thinkShaadi.heroCarousal = {
	carousalLinks: function() {
		$('.rsPlayBtnIcon').live('click',function(){
			//alert(1);
		})
	},
	init: function() {
		$('.royalSlider').royalSlider({
			arrowsNav: false,
			loop: false,
			keyboardNavEnabled: true,
			controlsInside: true,
			imageScaleMode: 'fill',
			arrowsNavAutoHide: false,
			autoScaleSlider: true,
			autoScaleSliderWidth: 960,
			autoScaleSliderHeight: 350,
			controlNavigation: 'bullets',
			thumbsFitInViewport: false,
			navigateByClick: true,
			startSlideId: 0,
			autoPlay: true,
			transitionType:'move',
			globalCaption: true,
			deeplinking: {
				enabled: true,
				change: false
			},
			video: {
				autoHideArrows: true,
				autoHideControlNav: false,
				autoHideBlocks: true
			}
		});
		thinkShaadi.heroCarousal.carousalLinks();
	}
};