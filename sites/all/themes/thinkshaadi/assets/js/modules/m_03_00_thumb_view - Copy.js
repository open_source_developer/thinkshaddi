/**
 * Created with JetBrains WebStorm.
 * User: Ashirwad
 * Date: 11/13/12
 * Time: 2:13 AM
 * To change this template use File | Settings | File Templates.
 */
/*jslint bitwise: true, eqeqeq: true, passfail: false, nomen: false, plusplus: false, undef: true, evil: true */
/*global window, document, $, jQuery, self, Modernizr */

// Define our root namespace if necessary
var thinkShaadi = window.thinkShaadi || {};

var url = baseUrl;
var next = 1;
var ajaxOn = 0;
var start = 0;
/**
 * @namespace Behaviour for the thumbnailView
 */

thinkShaadi.createThumbs = {
	//jsonURL: 'http://localhost/thinkshaddi/drupal_services/images/',
	getJson: function(jsonURL) {
		if(ajaxOn == 0){
			jQuery.ajax({
				type: "GET",
				url: jsonURL,
				dataType: "json",
				beforeSend: function(){
					ajaxOn = 1;
					jQuery('.loadMoreAjaxLoader').show();
				},
				success: function(data){
					ajaxOn = 0;
					start = start +1;
					jQuery('.loadMoreAjaxLoader').hide();
					var results = [];
					if(data.next == 1){
						next = 1;
						jQuery('.loadMoreAjaxLoader').text('').append('<img src="/thinkshaddi/sites/all/themes/thinkshaadi/assets/img/loader.gif" />');
					}else{
						next = 0;
						url = "";
						jQuery('.loadMoreAjaxLoader img').hide();
						jQuery('.loadMoreAjaxLoader').show().append('No more results found.');
					}
					
					if(data.result.length > 0){
						jQuery.each(data.result, function (key, val) {
						   	  var imagesString = '<li id="box'+key+'" class="box thumb" rel="'+val.url+'" style="display:none">' +
							'<img alt="'+val.category+'" src="'+val.img+'" width="208" />' +
							'<h3>'+val.vendor+'</h3>' +
							'<h2>'+val.category+'</h2>' +
							'<div class="hoverState">' +
							'<p>'+val.description+'</p>' +
							'<ul>' +
							'<li>' +
							'<a class="btnLike" href="#" title="'+val.likes+'">Like it</a>' +
							'</li>' +
							'<li>' +
							'<a class="btnFav" href="#" title="'+val.favorites+'">favorite it</a>' +
							'</li>' +
							'<li class="last">' +
							'<a class="btnAdd" href="#" title="Add it">Add it</a>' +
							'</li>' +
							'</ul>' +
							'</div>' +
							'</li>'
							results.push(imagesString);
						});
						if(start == 1){
							jQuery('.thumbList').html('');
						}
						jQuery(results).appendTo('.thumbList');
						thinkShaadi.arrange.init();
						thinkShaadi.loadImageSequence.init();
					}else{
						url = '';
						jQuery('.loadMoreAjaxLoader img').hide();
						jQuery('.loadMoreAjaxLoader').show().append('No more results found.');
					} 
					
				},
				error: function (msg) {

				}
			});	
		}
		
	},
	init: function() {
		url = baseUrl;
		if(jQuery('body').hasClass('front')){
			getUrl();
		}else{
			url = url+"images";
		}
		
  		  if(params !== ""){
			url = url + "?"+params+"&page="+start;
		}else{
			url = url + "?page="+start;
		}
		
		if(url !== ""){
			thinkShaadi.createThumbs.getJson(url);
		}
	}
};

thinkShaadi.arrange = {
	arrangeBoxes: function($container) {
		var thumbs = $container.find('.box');
		$container.imagesLoaded( function(){
			if(start == 1){
				jQuery('ul.thumbList li.box:hidden').show();
				$container.masonry({
					itemSelector : thumbs,
					columnWidth: 10,
					isAnimated: false
				});
			}else{
				$container.masonry({
					itemSelector : thumbs,
					columnWidth: 10,
					isAnimated: false
				});
				var $newElems = 	jQuery('ul.thumbList li.box:hidden');
				jQuery('ul.thumbList li:hidden').show();
				$container.masonry( 'appended', $newElems, true);
			}

		});
	},
	init: function(html) {
		thinkShaadi.arrange.arrangeBoxes(jQuery('.thumbList'),html);
	}
};

thinkShaadi.loadImageSequence = {
	imageSequence: function(obj,indx,delay){
		if(indx < jQuery(obj).length) {
			jQuery(obj).eq(indx).fadeIn(delay, function (){
				indx++;
				thinkShaadi.loadImageSequence.imageSequence(obj,indx,delay);
			});
		}
	},
	init: function(){
		var thumbs = jQuery(".categoryResults > ul > li");
		//thumbs.hide();
		//thinkShaadi.loadImageSequence.imageSequence(thumbs,0,500);
		thinkShaadi.thumnailView.thumbDetailSetup(jQuery('.categoryResults > ul > li'));
		thinkShaadi.lightBox.setup();
	}
};

thinkShaadi.thumnailView = {
    thumbSelector: jQuery('.categoryResults > ul > li'),
    thumbImage: jQuery('.categoryResults > ul > li').find('img'),

    thumbDetailSetup: function(selector) {
        selector.each(function() {
            thinkShaadi.thumnailView.bindEvents(jQuery(this), 300);
        })
    },
    bindEvents: function(thumb, duration) {
        thumb.bind({
            mouseenter: function() {
                var hoverState = jQuery(this).find('.hoverState');
                if(jQuery(this).height() > 100 ){
                    thinkShaadi.thumnailView.setHeightAndWidthToDetails(hoverState);
                } else {
                    return false;
                }
                thumb.find('.hoverState').stop(true, true).fadeIn(duration);
            }, mouseleave: function (){
                thumb.find('.hoverState').stop(true, true).fadeOut(duration);
            }
        });
		thumb.bind('click', function(){
			var detailImgUrl = thumb.attr('rel');
			window.location.href = detailImgUrl;
		});
    },
    setHeightAndWidthToDetails: function(setMe) {
        var parent = setMe.parent('li').find('img'),
            height = parent.height()-20,
            width = parent.width()-20;
        setMe.css({
            height: height,
            width: width
        });
    },
    init: function() {
        if(this.thumbImage.length !== 0){
            this.thumbDetailSetup(this.thumbSelector);
        }
    }
};

function getUrl(){
	var selected = jQuery('.categoryWrapper ul li.active a').attr('class');
	switch(selected){
	  case 'catFeatured':
	  case 'catPopular':
	    url = url +"images";
	  break;
	  case 'catCategory':
	  	url = url+'categories';
	  break;
	  default:
	    url = baseUrl;
      break;	
	}
}

jQuery(window).scroll(function(){
		if(jQuery(window).scrollTop() == jQuery(document).height() - jQuery(window).height()){
			if(jQuery('.thumbList').length !== 0){
				if(url != ''){
					thinkShaadi.createThumbs.init();
				}
			} else {
				return false;
			}
		}

});