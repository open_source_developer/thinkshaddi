(function($) {
    var map;
    var marker,i;
    var markerArray = new Array();
    $.fn.createMap = function($pins) {

      if($pins == 0){
        $("#map-canvas").html("No pins found to display");
      }else{
        if(typeof(map) != "object"){
          var mapOptions = {
            center: new google.maps.LatLng(65.42538424814593,-9.158204197883606),
            zoom: 2,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          var mapRes = $("#map-canvas");
          
           map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        }else{
           map.setZoom(3);
          // map.setCenter(new google.maps.LatLng(9.102097,-0.703125));
          var len = markerArray.length;
          for(i=0;i<len;i++){
            markerArray[i].setMap(null);
          }
        }
       
        
        var infowindow = new google.maps.InfoWindow();
        var obj = jQuery.parseJSON($pins);
        var content = "";
        i=0;
        //var bounds = new google.maps.LatLngBounds();
         $.each(obj, function (key, val) {
             var userLatLong = new google.maps.LatLng(val.lat,val.lng);
              
              marker =  new google.maps.Marker({
                  position: userLatLong,
                  map: map,
                  animation : google.maps.Animation.DROP
              });
              markerArray[i] = marker;
              google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                  map.setZoom(13);
                  map.setCenter(new google.maps.LatLng(obj[i].lat, obj[i].lng));
                  infowindow.setContent(obj[i].addr);
                  infowindow.open(map, marker);

                }
              })(marker, i));

              i++;
             // bounds.extend(userLatLong);
         });
         // map.fitBounds(bounds);
      }  
      
      
    };
})(jQuery);

