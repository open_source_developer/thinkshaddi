/**
 * Created with JetBrains WebStorm.
 * User: Ashirwad
 * Date: 11/13/12
 * Time: 5:48 AM
 * To change this template use File | Settings | File Templates.
 */
/*jslint bitwise: true, eqeqeq: true, passfail: false, nomen: false, plusplus: false, undef: true, evil: true */
/*global window, document, $, jQuery, self, Modernizr */

// Define our root namespace if necessary
var thinkShaadi = window.thinkShaadi || {};

/**
 * @namespace Behaviour for the footer show-hide
 */

thinkShaadi.footer = {
    footerSelector: '.footer',
    closeButton: '.footer .btnClose',
    footerSetup: function() {
        jQuery(this.footerSelector).hide();
        jQuery(window).scroll(function() {
            if(jQuery(window).scrollTop() + jQuery(window).height() == jQuery(document).height()) {
                jQuery(thinkShaadi.footer.footerSelector).show();
            }
        });
        thinkShaadi.footer.closeFooter(thinkShaadi.footer.closeButton, thinkShaadi.footer.footerSelector);
        thinkShaadi.footer.backToTop(jQuery('.linkBackToTop'));
		jQuery(this.footerSelector).prev().css('padding-bottom','240px');
    },
    closeFooter: function(btn, wrapper) {
        jQuery(btn).click(function(event) {
            event.preventDefault();
            jQuery(wrapper).slideUp();
        })
    },
    backToTop: function(link) {
        jQuery(link).click(function(event) {
            event.preventDefault();
            jQuery('body,html').animate({
                scrollTop: 0
            },function(){});
        })
    },
    init: function() {
        jQuery('.linkAboutUs').click(function(e){
            e.preventDefault();
            jQuery(thinkShaadi.footer.footerSelector).show();
        });
        thinkShaadi.footer.footerSetup();
    }
};