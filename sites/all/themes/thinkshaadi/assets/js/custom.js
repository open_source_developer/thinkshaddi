/**
 * Created with JetBrains WebStorm.
 * User: Ashirwad
 * Date: 11/4/12
 * Time: 2:22 AM
 * To change this template use File | Settings | File Templates.
 */
/*jslint bitwise: true, eqeqeq: true, passfail: false, nomen: false, plusplus: false, undef: true, evil: true */
/*global window, document, $, jQuery, self, Modernizr */

// Define our root namespace if necessary
var thinkShaadi = window.thinkShaadi || {};

thinkShaadi.lightBox = {
	wWidth: 0,
	wHeight: 0,
	lightBoxStructure: '<div class="lightBoxWrapper"></div><div class="lightBoxInner"></div>',
	lightBoxWrapper: '.lightBoxWrapper',
	lightBoxInner: '.lightBoxInner',
	innerBox:"",
	lightBoxLinks: {
		open: '.linkToggleView, .linkLoin, .linkSignUp',
		close: '.btnClose, .lightBoxWrapper'
	},

	setup: function() {
		thinkShaadi.lightBox.bindEvents();
	},
	alignToMiddle: function(innerBox) {
		if (jQuery.browser.msie) {
			this.wWidth = document.documentElement.offsetWidth;
			this.wHeight = document.documentElement.offsetHeight;

		} else {
			this.wWidth = window.innerWidth;
			this.wHeight = window.innerHeight;
		}
		var innerWidth = innerBox.width(),
			innerHeight = innerBox.height(),
			calculatedLeft = (this.wWidth/2)-(innerWidth/2),
			calculatedTop = (this.wHeight/2)-(innerHeight/2);

		if(this.wHeight < innerBox.height()){
			calculatedTop = calculatedTop+50;
		}

		innerBox.css({
			top: calculatedTop+50,
			left: calculatedLeft
		});
	},

	bindEvents: function(){
		jQuery(this.lightBoxLinks.open).click(function(event) {
			event.preventDefault();
			thinkShaadi.lightBox.createLightBox(jQuery(this));
		});
		jQuery(window).bind("resize", function () {
			thinkShaadi.lightBox.alignToMiddle(jQuery(thinkShaadi.lightBox.lightBoxInner));
		});
	},

	createLightBox: function(links) {
		var url = links.attr('rel'),
			body = jQuery('body');

		body.append(thinkShaadi.lightBox.lightBoxStructure);

		jQuery(thinkShaadi.lightBox.lightBoxWrapper).fadeIn(function(){
			jQuery(window).trigger('resize');
			jQuery(thinkShaadi.lightBox.lightBoxInner).fadeIn();

		});
		if(url.indexOf('img') === -1) {
			thinkShaadi.lightBox.alignToMiddle(jQuery(thinkShaadi.lightBox.lightBoxInner).addClass('lightBoxInnerPages').load(url));
		} else {
			thinkShaadi.lightBox.alignToMiddle(jQuery(thinkShaadi.lightBox.lightBoxInner).html('<img height="500" src="'+url+'" />'));
		}

		//set up lightBox close functionality
		jQuery(this.lightBoxLinks.close).live('click', function(e){
            e.preventDefault();
			thinkShaadi.lightBox.closeLightBox();
		});

		body.keydown(function(event) {
			if (event.which == 27) {
				thinkShaadi.lightBox.closeLightBox();
			}
		});

	},
	closeLightBox: function() {
		jQuery(thinkShaadi.lightBox.lightBoxInner).fadeOut(function() {
			jQuery(thinkShaadi.lightBox.lightBoxWrapper).fadeOut('slow', function(){
				jQuery(this).remove()
			});
			jQuery(thinkShaadi.lightBox.lightBoxInner).remove()
		});
		jQuery('body').removeClass('lightBoxActive');
	},
	init: function() {
		//thinkShaadi.lightBox.setup();
	}
};

thinkShaadi.tabs = {
	commonTabs: function (tabWrapper){
		var tabs = jQuery(tabWrapper).find('.tabs li a'),
			tabContent = jQuery(tabWrapper).find('.tabContent > li');
		jQuery('.tabContent > li').hide().eq(0).show().css('display','block');
		jQuery(tabs).each(function(){
			jQuery(this).click(function(e){
				e.preventDefault();
				if(jQuery(this).parent().hasClass('active')){
					return false;
				} else {
					jQuery('.tabs li').removeClass('active');
					jQuery(this).parent().addClass('active');
					var idx = jQuery(this).parent().index();
					jQuery('.tabContent > li').hide();
					jQuery(tabContent).eq(idx).show().css('display','block');
				}
			})
		})
	},
	init: function(){
		if(jQuery('.fnTabs').length !== 0){
			thinkShaadi.tabs.commonTabs('.fnTabs');
		}
	}
};

thinkShaadi.equalHeight = {
	calculateHeight: function(parent){
		if(jQuery(parent).length !== 0){
			jQuery(parent).css('border','4px solid red');
		}
	},
	init: function(){
		jQuery('.linkLoin').click(function(e){
            e.preventDefault();
			thinkShaadi.equalHeight.calculateHeight('.fnEqualHeight');
		});

	}
};

thinkShaadi.selectBoxStyle = {
	init: function(){
		jQuery("select").selectBoxIt({

			//theme
			theme: 'jqueryui',

			// Uses the jQuery 'fadeIn' effect when opening the drop down
			showEffect: "fadeIn",

			// Sets the jQuery 'fadeIn' effect speed to 400 milleseconds
			showEffectSpeed: 400,

			// Uses the jQuery 'fadeOut' effect when closing the drop down
			hideEffect: "fadeOut",

			// Sets the jQuery 'fadeOut' effect speed to 400 milleseconds
			hideEffectSpeed: 400

		});
	}
};

thinkShaadi.formAddMore = {
	addMoreButton: '.btnAddMore',
	removeEntryButton: '.removeEntry',
	appendData : function(data){
		var newVal = data.prev('input').val();
		if(newVal !== ''){
			data.siblings('.newEntry').append('<li>'+newVal+'<a href="#" class="removeEntry">Remove</a></li>');
		}
	},
	removeData: function(data){
		data.parent('li').remove();
	},
	setup: function(){
		thinkShaadi.formAddMore.bindEvents();
	},
	bindEvents: function () {
		jQuery(this.addMoreButton).live('click', function(event){
			event.preventDefault();
			thinkShaadi.formAddMore.appendData(jQuery(this));
		})
		jQuery(this.removeEntryButton).live('click', function(event){
			event.preventDefault();
			thinkShaadi.formAddMore.removeData(jQuery(this))
		})
	},
	init: function() {
		thinkShaadi.formAddMore.setup();
	}
}

thinkShaadi.primarynav = {
	getActiveLink: function(){
		var activeLink = jQuery('.primaryNavigation li a'),
			linkParent = jQuery(jQuery(activeLink).parent('li'));
			
			jQuery(activeLink).each(function(){
				if(jQuery(this).hasClass('active')){
				}
			});
			
		jQuery(activeLink).each(function () {
			jQuery(this).click(function(){
				jQuery(linkParent).removeClass('activeLink');
				jQuery(this).parent('li').addClass('activeLink');
			})
		})
	},
	init: function() {
		thinkShaadi.primarynav.getActiveLink();
	}
}
/*
 * SETUP DOM READY AND LOAD EVENTS HERE
 */

 function doChosen(selector) {
 	if(typeof jQuery.fn.chosen !== 'undefined'){
 		jQuery(selector).trigger("liszt:updated");
 	}
    
   // jQuery(selector).chosen({allow_single_deselect:true});
}
(function($) {
$.fn.doChosen = function($selector) {
	
	if(typeof jQuery.fn.chosen !== 'undefined'){
		
		jQuery($selector).trigger("liszt:updated");
    	jQuery($selector).chosen();
	}
    
};
    $.fn.scrollToError = function($selector) {
        jQuery('body,html').animate({
            scrollTop: 0
        });
    };
})(jQuery);
jQuery(document).ready(function () {

   	if(jQuery("#serviceCountry #service-country-select").length > 0){
   	 jQuery("#serviceCountry #service-country-select").change(function(){
	    var val = jQuery(this).val();
	    if(val != ""){
	      var url = baseUrl+"country-filter?country="+val;
	      jQuery(this).attr('disabled',true);		
	      jQuery.getJSON(url,
				function(data) { 
				  jQuery("#service-country-select").attr('disabled',false);	
				  var selectedVals = jQuery("#service-city-select").val();
				 	
				 	jQuery("#service-city-select option").remove();
				 	var selected="";	
				  jQuery.each(data.cityOptions, function(i,item){
				  	selected = "";
					if(typeof i === 'undefined' || typeof item === 'undefined'){
					   // your code here.
					   
					 }else{
					   if(jQuery.inArray(i, selectedVals) != -1){
					   	 selected = "selected='selected'";
					   }
					   jQuery("#service-city-select").prepend("<option value='"+i+"' "+selected+">"+item+"</option>");

					 }
				  });
				  doChosen("#service-city-select");
				   var selectedVals = jQuery("#service-state-select").val();
				 	
				 	jQuery("#service-state-select option").remove();
				 	var selected="";
				  jQuery.each(data.stateOptions, function(i,item){
				  	selected = "";
					if(typeof i === 'undefined' || typeof item === 'undefined'){
					   // your code here.
					   
					 }else{
					   if(jQuery.inArray(i, selectedVals) != -1){
					   	 selected = "selected='selected'";
					   }
					   jQuery("#service-state-select").prepend("<option value='"+i+"' "+selected+">"+item+"</option>");

					 }
				  });
				  	doChosen("#service-state-select");	
				 
				  
		  });
	    }
	  });

   }

   if(jQuery("#serviceCountry #service-state-select").length > 0){
   	 jQuery("#serviceCountry #service-state-select").change(function(){
	    var val = jQuery(this).val();
	    if(val != ""){
	      var url = baseUrl+"state-filter?state="+val;
	      jQuery(this).attr('disabled',true);		
	      jQuery.getJSON(url,
				function(data) { 
				  jQuery("#service-state-select").attr('disabled',false);	
				  var selectedVals = jQuery("#service-city-select").val();
				 	
				 	jQuery("#service-city-select option").remove();
				 	var selected="";	
				  jQuery.each(data.option, function(i,item){
				  	selected = "";
					if(typeof i === 'undefined' || typeof item === 'undefined'){
					   // your code here.
					   
					 }else{
					   if(jQuery.inArray(i, selectedVals) != -1){
					   	 selected = "selected='selected'";
					   }
					   jQuery("#service-city-select").prepend("<option value='"+i+"' "+selected+">"+item+"</option>");

					 }
				  });
				  
				  	doChosen("#service-city-select");	
				 
				  
		  });
	    }
	  });

   }	
   	  jQuery("#category-select").live('change',function(){
	    var val = jQuery(this).val();
	    if(val != ""){
	      var url = baseUrl+"subcat?category="+val;
	      jQuery(this).attr('disabled',true);		
	      jQuery.getJSON(url,
				function(data) {
				  jQuery("#category-select").attr('disabled',false);	
				  var selectedVals = jQuery("#subcategory-select").val();
				 	//console.log(selectedVals);
				 	jQuery("#subcategory-select option").remove();
				 	var selected="";
				 	console.log(data.option);	
				  jQuery.each(data.option, function(i,item){
				  	selected = "";
					if(typeof i === 'undefined' || typeof item === 'undefined'){
					   // your code here.
					   
					 }else{
					   if(jQuery.inArray(i, selectedVals) != -1){
					   	 selected = "selected='selected'";
					   }
					   jQuery("#subcategory-select").prepend("<option value='"+i+"' "+selected+">"+item+"</option>");

					 }
				  });
				  
				  	doChosen("#subcategory-select");	
				  
		  });
	    }
	  });
   
//    Vendor form edit - START
    jQuery('.profileFormDiv').hide();
    jQuery('.userEditHref').click(function(e){
        e.preventDefault();
        jQuery(this).hide();
        jQuery('.detailsDiv').fadeOut(function(){
            jQuery('.profileFormDiv').fadeIn();
        });
    });
    jQuery('.cancel').click(function(e){
        jQuery('.profileFormDiv').fadeOut(function(){
            jQuery('.detailsDiv').fadeIn();
            jQuery('.userEditHref').show();
        });
    })
	
	if(jQuery('.addNewLocation').length > 0){
		
	  jQuery('.addNewLocation').click(function(e){
		 e.preventDefault();
	     var count = jQuery('.primaryWrapperDiv:visible').length;
		 
		 var next = count+ 1;
		 
		 var id = "primaryWrapper_" + next.toString();
		 
		 jQuery('.'+id).show();
		 //alert(jQuery('.primaryWrapperDiv:visible').length);
		 if(jQuery('.primaryWrapperDiv:visible').length >= 5){
			$(".addNewLocation").parent("div").hide();
		 }
	  });
	}

// Vendor Form Edit - END

	//jQuery('.loadMoreAjaxLoader').hide();
	thinkShaadi.primarynav.init();
	if(jQuery('.categoryResults').length !== 0){
		thinkShaadi.createThumbs.init();
	}

//      thinkShaadi.heroCarousal.init();
	if(jQuery('.fnTabs').length !== 0){
		thinkShaadi.tabs.init();
	}
	thinkShaadi.formAddMore.init();
	//thinkShaadi.selectBoxStyle.init();
	jQuery('.categoryWrapper ul li a').click(function(){
        jQuery('.categoryResults ul').remove();
		jQuery('.loadMoreAjaxLoader').text('').append('<img src="sites/all/themes/thinkshaadi/assets/img/loader.gif" />');
		start = 0;
		if(jQuery(this).parents('div').hasClass('subMenu')) {
			jQuery('.categoryWrapper .subMenu li').removeClass('active');
		} else {
			jQuery('.categoryWrapper ul li').removeClass('active');
		}

		jQuery(this).parent('li').addClass('active');

		if(jQuery(this).hasClass('catCategory')){
			thinkShaadi.createCategoryThumbs.init();
		} else {
			jQuery('.categoryResults > .categoryList').remove();
			jQuery('.hasSubMenu').animate({
				'paddingBottom': 60
			});
			jQuery('.subMenu').fadeIn()//.find('li').eq(0).addClass('active');

			jQuery('.categoryResults').append('<ul class="thumbList"></ul>');
			thinkShaadi.createThumbs.init();
		}
	});
    if(jQuery('.footer').length !== 0){
        thinkShaadi.footer.init();
    } else {

    }

    jQuery('.bundleResults ul > li').each(function(){
        jQuery(this).find('.thumbCollection').click(function() {
            var bundleDetails = jQuery(this).parent().attr('rel');
            window.location.href = bundleDetails;
        })
    })

    jQuery('.ctools-use-modal-processed, .uploadTabs > li > a').click(function(){
        jQuery(window).trigger('resize');
    });

    if(jQuery(".invitationPanel .btn_user").length > 0){
    	 jQuery(".invitationPanel .btn_user").click(function(e){
    	 	e.preventDefault();
             jQuery('.status').fadeOut();
	      	jQuery("a.active").removeClass("active");
	        jQuery(".vendorSignup").hide();
	        jQuery(this).addClass("active");
	        jQuery(".userSignup").show();
			jQuery(".errorDiv").html('');
			jQuery(".succDiv").html('');
			jQuery(":text").val('');
	    });
    }
    if(jQuery(".invitationPanel .btn_vendor").length > 0){
		 jQuery(".invitationPanel .btn_vendor").click(function(e){
		 	e.preventDefault();
             jQuery('.status').fadeOut();
	        jQuery("a.active").removeClass("active");
	        jQuery(this).addClass("active");
	        jQuery(".userSignup").hide();
	        jQuery(".vendorSignup").show();
			jQuery(".errorDiv").html('');
			jQuery(".succDiv").html('');
			jQuery(":text").val('');
	    });
    }

    if(jQuery('.welcomeBanner').length !== 0){
        jQuery('.welcomeBanner').parents('.wrapper').css('width','100%');
    }

    if(jQuery('.boardContent').length !== 0){
        jQuery('.boardContent > ul > li:nth-child(3n)').css('margin-right', '0');

    }
    if(jQuery('.addNewBundle a, .edit, .changePassword, .linkVendorsSubmit, .views-field a').length !== 0){
        jQuery('.addNewBundle a, .edit, .changePassword,.linkVendorsSubmit, .views-field a').click(function(){
            jQuery('body,html').animate({
                scrollTop: 0
            });
        })
    }

//    Color pallet OPEN

    jQuery('.linkToggleViewColor').click(function(e){
        e.preventDefault();
        var colorPallet = jQuery('.colorOptions'),
            $this = jQuery(this);
        colorPallet.animate({
            left: '-85px'
        },function(){
            jQuery($this).removeClass('linkToggleViewColor').addClass('btnCloseColor');
        });
    });

//    Color pallet CLOSE

    jQuery('.btnCloseColor').live('click', function(e){
        e.preventDefault();
        var colorPallet = jQuery('.colorOptions'),
            $this = jQuery(this);
        colorPallet.animate({
            left: '-461px'
        },function(){
            jQuery($this).removeClass('btnCloseColor').addClass('linkToggleViewColor');
        });
    });

//    var message = "Sorry! This function is disabled.";
//    function rtclickcheck(keyp){
//        if (navigator.appName == "Netscape" && keyp.which == 3){alert(message); return false;}
//        if (navigator.appVersion.indexOf("MSIE") != -1 && event.button == 2){alert(message); return false;}}
//    document.onmousedown = rtclickcheck;

//    Desable right click

    function nrcIE() {
        if (document.all) {
            return false;
        }
    }

    function nrcNS(e) {
        if (document.layers || (document.getElementById && ! document.all)) {
            if (e.which == 2 || e.which == 3) {
                return false;
            }
        }
    }

    if (document.layers) {
        document.captureEvents(Event.MOUSEDOWN);
        document.onmousedown = nrcNS;
    }else {
        document.onmouseup = nrcNS;
        document.oncontextmenu = nrcIE;
    }
    document.oncontextmenu = new Function("return false");
});