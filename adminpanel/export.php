<?php
include_once 'config.php';
include_once 'classes/class.DatabaseI_System.php';
include_once 'classes/class.user.php';
$objUser = new User();

$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
$reportName ="Report_export_".date("Y-F-d_h-i").".xls";
$sort = '';$orderBy ='';$search=null;
if(isset($_GET['orderby']) && !empty($_GET['orderby'])){
	$orderBy = trim($_GET['orderby']);
}else{
	$orderBy = '';
}
if(isset($_GET['sort']) && (trim($_GET['sort']) == "ASC" || trim($_GET['sort']) == "DESC")){
	$sort = trim($_GET['sort']);
}else{
	 $sort = "ASC";
}

if(isset($_REQUEST['search'])){

    $dateError =0;
    if(isset($_REQUEST['dtfrm']) && !empty($_REQUEST['dtfrm']) && $_REQUEST['dtfrm'] != 'From Date'){
            $date  = trim($_REQUEST['dtfrm']);
            $dateEx = explode('-',$date);
            if(count($dateEx) == 3){
                    if((strlen($dateEx[1]) == 2 || strlen($dateEx[1]) == 1) && (strlen($dateEx[0]) == 2 || strlen($dateEx[0]) == 1) && strlen($dateEx[2]) == 4){
                            if(checkdate($dateEx[1],$dateEx[0],$dateEx[2])){
                                    $from_date = $dateEx[2]."-".$dateEx[1]."-".$dateEx[0];
        $from = strtotime($from_date);
                            }else{
                                    $dateError =1;
                            }
                    }else{
                            $dateError =1;
                    }
            }else{
                    $dateError =1;
            }
    }
    if(isset($_REQUEST['dttill']) && !empty($_REQUEST['dttill']) && $_REQUEST['dttill'] != 'Till Date'){
            $date  = trim($_REQUEST['dttill']);
            $dateEx = explode('-',$date);
            if(count($dateEx) == 3){
                    if((strlen($dateEx[1]) == 2 || strlen($dateEx[1]) == 1) && (strlen($dateEx[0]) == 2 || strlen($dateEx[0]) == 1) && strlen($dateEx[2]) == 4){
                            if(checkdate($dateEx[1],$dateEx[0],$dateEx[2])){
                                    $till_date = $dateEx[2]."-".$dateEx[1]."-".$dateEx[0];
        $till = strtotime($till_date);
                            }else{
                                    $dateError =1;
                            }
                    }else{
                            $dateError =1;
                    }
            }else{
                    $dateError =1;
            }
    }

    if($dateError == 0){
            if(isset($till) && isset($from) && $till >= $from){
                    $search['date']['from'] = $from_date;
                    $search['date']['till'] = $till_date;
            }elseif(isset($till)){
                    $search['date']['from'] = '2011-11-30';
                    $search['date']['till'] = $till_date;
            }elseif(isset($from)){
                    $search['date']['from'] =  $from_date;
                    $search['date']['till'] = date('Y-m-d');
            }
    }

}
switch(strtolower($type)){
	case 'user' :
		
		$res = $objUser->get_user_form_details_csv($orderBy,$sort,$search);
		$reportName ="User_Form_Report_export_".date("Y-F-d_h-i").".xls";;
	break;
	case 'vendor':
		$res = $objUser->get_vendor_form_details_csv($orderBy,$sort,$search);
		$reportName ="Vendor_Report_export_".date("Y-F-d_h-i").".xls";
	break;
	
}
header("Content-type: application/x-msdownload");
header("Content-Disposition: attachment; filename=".$reportName."");
header("Pragma: no-cache");
header("Expires: 0");

$search = array('\r','\n',chr(10),chr(13));
$replace = array('','','','','','','');

if(!isset($res) || $res === false){
	
}else{
	$columns = $res->getFieldNames();
	$columnCount = count($columns);
	$column ="";
	for($i=0;$i<$columnCount;$i++){
		$column .= $columns[$i]->name."\t";
	}
	$column .= "\n";
	$data = $res->getResultToArray();
	//print_r($data[0]);
	$dataCount= count($data);
	$dataColumn ="";
	for($i=0;$i<$dataCount;$i++){
		$columnData = implode($data[$i],"\t");
		$dataColumn .= str_replace($search, $replace, $columnData)."\n";
	}
	
	echo $column;
	echo $dataColumn;
}
?>