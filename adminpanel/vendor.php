<?php 
	session_start();
	include('config.php');
	include_once 'email.php';
	if(!isset($_SESSION['uname']))
	{
		header('Location:login.php');
	}
	$orderBy= '';
	$page =1;
	$search = null;
	$dateError = 0;
	include_once 'classes/class.DatabaseI_System.php';
	include_once 'classes/class.user.php';
	include_once 'classes/class.Pagination.php';
	 $mailCount = 0;
	 $objUser = new User();
	 	if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete'){
	 		if(isset($_REQUEST['id']) && !empty($_REQUEST['id'])){
	 			$id = $_REQUEST['id'];
	 			$objUser->deleteVendor($id);
	 		}

	 	}
        if(isset($_POST['submit'])){
        	//print_r($_POST['user-checkbox']);
        	include_once 'phpmailer/class.phpmailer.php';
        	$count = count($_POST['user-checkbox']);
        	$subject = "Mail For Vendors";

        	for($i=0;$i<$count;$i++){
        		$mail = new PHPMailer(true);
        		$email = $_POST['user-checkbox'][$i];
        		$random = substr(number_format(time() * rand(),0,'',''),0,10);
        		$url = "http://".$_SERVER['HTTP_HOST']."/vendor/register/".$random;
        		$message = "You're one of the first vendors to be listed on Think Shaadi. We are still putting the final touches on the website and would love to hear your feedback.".chr(10).chr(13);
        		$message .= "As a vendor on Think Shaadi you can list your images and videos and update your profile page at any time.".chr(10).chr(13);
        		$message .= "Get started now ( ".$url." ) .".chr(10).chr(13);
        		$message .= "Happy posting!".chr(10).chr(13);
        		$message .= "XX".chr(10).chr(13);
        		$message .= "The Think Shaadi team  ".chr(10).chr(13);
        		$mail->ContentType = 'text/plain'; 
		        $mail->IsHTML(false);
		        $mail->IsSMTP();
		        $address =  $_POST['user-checkbox'][$i];
		        $mail->AddAddress($address);

		        $mail->SetFrom('noreply@thinkshaadi.com','Think Shaadi');

		        //$mail->AddReplyTo('');



		        $mail->Subject = "You're in! ";
		        //$mail->From = ;  

		        $mail->Body = $message;


		        if($mail->Send()){
		            $objUser->updateVendor($email,$random);
        			$mailCount++;
		        }
        		
        	}
        }
		if(isset($_REQUEST['search'])){
			
			if(isset($_REQUEST['dtfrm']) && !empty($_REQUEST['dtfrm']) && $_REQUEST['dtfrm'] != 'From Date'){
				$date  = trim($_REQUEST['dtfrm']);
				$dateEx = explode('-',$date);
				if(count($dateEx) == 3){
					if((strlen($dateEx[1]) == 2 || strlen($dateEx[1]) == 1) && (strlen($dateEx[0]) == 2 || strlen($dateEx[0]) == 1) && strlen($dateEx[2]) == 4){
						if(checkdate($dateEx[1],$dateEx[0],$dateEx[2])){
							$from_date = $dateEx[2]."-".$dateEx[1]."-".$dateEx[0];
                            $from = strtotime($from_date);
						}else{
							$dateError =1;
						}
					}else{
						$dateError =1;
					}
				}else{
					$dateError =1;
				}
			}
			if(isset($_REQUEST['dttill']) && !empty($_REQUEST['dttill']) && $_REQUEST['dttill'] != 'Till Date'){
				$date  = trim($_REQUEST['dttill']);
				$dateEx = explode('-',$date);
				if(count($dateEx) == 3){
					if((strlen($dateEx[1]) == 2 || strlen($dateEx[1]) == 1) && (strlen($dateEx[0]) == 2 || strlen($dateEx[0]) == 1) && strlen($dateEx[2]) == 4){
						if(checkdate($dateEx[1],$dateEx[0],$dateEx[2])){
							$till_date = $dateEx[2]."-".$dateEx[1]."-".$dateEx[0];
                            $till = strtotime($till_date);
						}else{
							$dateError =1;
						}
					}else{
						$dateError =1;
					}
				}else{
					$dateError =1;
				}
			}
			
			if($dateError == 0){
				if(isset($till) && isset($from) && $till >= $from){
					$search['date']['from'] = strip_tags(trim($from_date));
					$search['date']['till'] = strip_tags(trim($till_date));
				}elseif(isset($till)){
					$search['date']['from'] = '201-02-28';
					$search['date']['till'] = strip_tags(trim($till_date));
				}elseif(isset($from)){
					$search['date']['from'] =  strip_tags(trim($from_date));
					$search['date']['till'] = date('Y-m-d');
				}
			}
			
		}
		
        $objUser = new User();
        $countRes = $objUser->get_vendor_form_count($search=NULL);
		if($countRes){
			$totalrows = $countRes->getNext();
		}else{
			$totalrows['total'] = 0;
		}
        
        $Pagination = new Pagination();
        $limit = $CONFIG->perpage ;
        if(isset($_GET['page']) && is_numeric(trim($_GET['page']))){$page = trim($_GET['page']);}else{$page = 1;}
        $startrow = $Pagination->getStartRow($page,$limit);

        //create page links
        if($CONFIG->showpagenumbers == true){
                $pagination_links = $Pagination->showPageNumbers($totalrows['total'],$page,$limit);
        }else{$pagination_links=null;}

        if($CONFIG->showprevnext == true){
                $prev_link = $Pagination->showPrev($totalrows['total'],$page,$limit);
                $next_link = $Pagination->showNext($totalrows['total'],$page,$limit);
        }else{$prev_link=null;$next_link=null;}

        if(isset($_GET['orderby']) && !empty($_GET['orderby'])){
            $orderBy = strip_tags(trim($_GET['orderby']));
        }else{
            $orderBy = '';
        }
        if(isset($_GET['sort']) && (trim($_GET['sort']) == "ASC" || trim($_GET['sort']) == "DESC")){
            $sort = strip_tags(trim($_GET['sort']));
        }else{
             $sort = "ASC";
        }
		if($totalrows['total'] > 0){
			 $sampleRes = $objUser->get_vendor_form_details($limit,$startrow,$orderBy,$sort,$search);
			 if($sampleRes === false){
				$sampleData = false;
			 }else{
				$sampleData = $sampleRes->getResultToArray();
			 }
		}else{
			$sampleData = false;
		}
       
	/* 
	 * To change this template, choose Tools | Templates
	 * and open the template in the editor.
	 */

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Thinkshaadi</title>
    
    <script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script type="text/javascript" src="js/datetimepicker.js"></script>
    <script>
    	$(document).ready(function(){
    		$("a.delete").click(function(e){
    			var r=confirm("Are you sure you want to delete this entry??")
    			if(r == false){
					e.preventDefault();
    			}
    		});
    	});
    </script>
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="assets/css/ie/ie6.css" /><![endif]-->
	
</head>
<body>
    <div id="wrapper">
        <div id="container">
            <h1>
                <img src="images/logo.png" title="XENON XT" />
                <span class="hiddenText">Tata-Xenon</span>
            </h1>
            <?php
		   include_once "header.php";
             ?>
            <h2>
               Vendor Form           
            </h2>
			<!--<form method="get">
				<ul class="searchContainer">
					<li><input type="text" name="dtfrm" id="dtfrm" value="<?php if(isset($_REQUEST['dtfrm'])){ echo $_REQUEST['dtfrm'];}else{ echo "From Date"; } ?>" /><a href="javascript:NewCal('dtfrm','ddmmyyyy')"><img class="calender" src="images/cal.gif" width="22" height="23" border="0" alt="Pick a Date"></a></li>
					<li><input type="text" name="dttill" id="dttill" value="<?php if(isset($_REQUEST['dttill'])){ echo $_REQUEST['dttill'];}else{ echo "To Date"; } ?>" /><a href="javascript:NewCal('dttill','ddmmyyyy')"><img class="calender" src="images/cal.gif" width="22" height="23" border="0" alt="Pick a Date"></a></li>
					<li class="btnSearch"><input title="Search" name="search" type="submit" value="Search" /></li>
					<li class="btnSearch"><input title="Reset" name="reset" type="button" value="Reset" onClick="location.href='index.php'" /></li>
				</ul>
			</form>-->
			
            <div class="eventWrapper">
            	<div>
            		<?php
            			if(isset($_POST['submit']) && $mailCount > 0){
            				echo "<p>Mail Sent to ".$mailCount." Vendor(s)</p>" ;
            			}

            		?>
            	</div>
				<div class="info">
					Please click on Business Name,Email Id to sort the content in ascending or descending order respectively.
				</div>
			    <div>
                	<form method="post" >
                    <table class="eventDetails" id="eventTableSorter" cellpadding="0" cellspacing="0" border="1"  width="100%">
                        <thead>
                            <tr>
                              <th width="10" scope="col"><input type="checkbox" name="ucheckbox" id="user-checkbox" /></th>	
                              <th width="142" scope="col"><?php echo $Pagination->columnSortArrows('businessname','Business Name',$orderBy,$sort); ?></th>
                              <th width="67" scope="col"><?php echo $Pagination->columnSortArrows('email','Email Id',$orderBy,$sort); ?></th>
                              <th width="78" scope="col">Website</th>
							  <th width="78" scope="col"><?php echo $Pagination->columnSortArrows('insertdatetime','Contact Date',$orderBy,$sort); ?></th>
                               <th width="78" scope="col"><?php echo $Pagination->columnSortArrows('mailSentDateTime','Mail Sent Date',$orderBy,$sort); ?></th>
                               <th width="78" scope="col">Delete</th>
                            </tr>

                  </thead>
                        <tbody>
                           <?php
							$recordsFound = false;
                           if($sampleData) {
                                $j=1;
								$recordsFound = true;
                           	for($i=0;$i<count($sampleData);$i++) {
                                    
	                           ?>
	                            <tr>
	                              <td><input type="checkbox" name="user-checkbox[]" value="<?php echo $sampleData[$i]['email'];  ?>"  /></td>  
								  <td><?php echo $sampleData[$i]['businessname']; ?></td>
								  <td><?php echo $sampleData[$i]['email']; ?></td>
								  <td><?php echo $sampleData[$i]['website']; ?></td>
								  <td><?php echo $sampleData[$i]['datetime']; ?></td>
								  <td><?php if(!empty($sampleData[$i]['randomNo']) && !is_null($sampleData[$i]['randomNo'])){ echo $sampleData[$i]['mailSentDateTime'] ;}else{ echo "-"; }  ?></td>
								  <td>
								  	<?php if(empty($sampleData[$i]['uid'])): ?>
								  		<a href="vendor.php?action=delete&id=<?php echo $sampleData[$i]['id']; ?>" class="delete">Delete</a>
								  	<?php endif; ?>
								  </td>
								</tr>
                          
	                          <?php $j++;}
	                          ?>
	                          <tr>
	                          	<td align="left" valign="top" colspan="7">
                          			<input type="submit" name="submit" value="Send Mail" />
	                          	</td>
	                          </tr> 
	                          <?php
                           } else { $recordsFound = false; ?>
                           	    <tr>
	                                <td align="left" valign="top" colspan="7"><?php echo "No Record Found."; ?></td>
			              </tr>
                         <?php  } ?>
                        </tbody>
                    </table>
                	</form>
              </div>
                <?php
                    if(!($prev_link==null && $next_link==null && $pagination_links==null)){
                        echo '<div class="pagination">'."\n";
                        echo $prev_link;
                        echo $pagination_links;
                        echo $next_link;
                        echo '<div style="clear:both;"></div>'."\n";
                        echo "</div>\n";
                    }
                ?>
                <?php
					if($recordsFound){
						$query_string ='';
						if(!empty($_SERVER['QUERY_STRING'])){
							 $query_string = eregi_replace("page=[0-9]{0,10}","",$_SERVER['QUERY_STRING']);
						}
						?>
							 <a href="export.php?type=vendor&<?php echo $query_string; ?>" title="Export to Excel file" class="btn_expExl">
								<img src="images/btn_exportToExl.gif" alt="Export to Excel file" />
								<span class="hiddenText">Export to Excel file</span>
							</a>
						<?php
					}
				?>
               
            </div>
        </div>
    </div>
</body>
</html>
