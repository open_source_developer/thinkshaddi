<?php
session_start();
require_once('config.php');
include_once 'classes/class.DatabaseI_System.php';
include_once 'classes/class.user.php';
global $msg;
$objUser = new User();
$username = '';
$password = '';
$msg = "";
//print_r($_POST);
if(isset($_POST['submit']) && $_POST['submit'] == 'Login') {
	$username = strtolower($_REQUEST['uname']);
	
	
	$password = $_REQUEST['pass'];
	$flag = $objUser->checkLogin($username,$password);
	if($flag) {
                $_SESSION['uname'] = $username;
		header('Location:index.php');
	} else {
		$msg =  isset($objUser->errorMessage) ? $objUser->errorMessage : "";
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Thinkshaadi</title>
    <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="assets/css/ie/ie6.css" /><![endif]-->
</head>
<body>

    <div id="wrapper">
        <div id="container">
            <h1>
               <img src="images/logo.png" title="XENON XT" />
                <span class="hiddenText">Tata-Xenon</span>
            </h1>
            <h2>
                <img src="images/text_login.gif" alt="Login" />
                <span class="hiddenText">Login</span>            
            </h2>
             
            <div class="contentWrapper">
                <p>Please fill in the following details to access your
                account.<span class="reqNotification"><span>*</span> Fields are required</span></p><br />
                <p><div style="color:#FF0000; font-weight:bold"><?php if (isset($_POST['submit'])) { echo $msg; }?></div></p>
                <form id="login" name="login" method="post">
                   <label for="userName" title="User name">User name<span class="req">*</span></label>
                   <input type="text" id="userName"  name="uname"  />
                   <p class="errMsg">Please enter User name</p>
                   <label for="password" title="Password">Password<span class="req">*</span></label>
                   <input type="password" id="password" name="pass" />
                   <p class="errMsg">Please enter Password</p>
                   <p> 
                     <input class="btn_Login" style="background-color:red;color:white;font-weight:bold;" type="submit" name="submit" alt="Click to login" value="Login" />
                   </p>
                </form>
            </div>
        </div>
    </div>
</body>
</html>