<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$hostname = 'us-cdbr-azure-west-b.cleardb.com';
$username = "bcc1380781e36f";
$password = "aa7c3041";
$dbname   = "thinkshaadi";

$conn = @mysqli_connect($hostname, $username, $password);
if (!$conn) $error = 'Could not connect to database.';

$db_selected = @mysqli_select_db($conn,$dbname);
if (!$db_selected) $error = 'Could not select database.';

function printr_array($array){
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}


class DatabaseI_System {

    var $conn;
    var $error = 'No Error';
    var $sqlString = "";
    var $errNo = "";

    public function sanitize($sVar)
    {
        $sVar = strip_tags(trim($sVar));
        if (1 != get_magic_quotes_gpc()) {
            $sVar = mysqli_real_escape_string($sVar);
        }
        return $sVar;
    }

    public function escape_string($var){
        global $conn;
        return $conn->real_escape_string($var);
    }

    function query($sql) {
        global $conn;
        if (!$conn) {
            $this->error = 'Not connected to database.';
            return false;
        }

        $this->sqlString=$sql;
        $res = mysqli_query( $conn,$sql) or $this->query_error($sql,mysqli_error($conn));
        if (!$res) {
            $this->error = mysqli_error($conn);
            $this->errNo = mysqli_errno($conn);
            return false;
        }

        return new DatabaseI_Result_PassSystem($res);
    }



    function getError() {
        return $this->error;
    }

    function query_error($query,$error) {
         global $conn;
        
        @mysqli_query($conn,"rollback") or die(@$conn->error());
        @mysqli_query($conn,"Unlock tables") or die(@$conn->error());
        $query = $conn->real_escape_string($query);
        $error = $conn->real_escape_string($error);

        $file = $_SERVER["SCRIPT_NAME"];
        //$break = Explode('/', $file);
        //$pfile = $break[count($break) - 1];
        $error_log = "insert into ERROR_LOG(query,error,date) values('$query','$error',now())";
        @mysqli_query($conn,$error_log);
        
        
        
    }

    function start_trans() {
         global $conn;
        @mysqli_query($conn,"START TRANSACTION") or die(@$conn->error());
    }

    function comit_trans() {
         global $conn;
        @mysqli_query($conn,"COMMIT") or die(@$conn->error());
    }

    function rollback_trans() {
         global $conn;
        @mysqli_query($conn,"ROLLBACK") or die(@$conn->error());
    }
}


class DatabaseI_Result_PassSystem {
    var $conn;
    var $res;
    var $numRows;
    var $numAffected;
    var $insertId;

    function DatabaseI_Result_PassSystem(&$res) {
        $this->res = $res;

    }

    function getNext() {
        $res = @mysqli_fetch_assoc($this->res);
        return $res;
    }

    function getNumRows() {
        $res = ($this->numRows ? $this->numRows : @mysqli_num_rows($this->res));
        return $res;
    }

    function getNumAffected() {
        global $conn;
        $res = ($this->numAffected ? $this->numAffected : @mysqli_affected_rows($conn));
        return $res;
    }

    function getResult() {
        return $this->res;
    }

    function getResultToArray(){
        //MYSQL_ASSOC, MYSQL_NUM, and MYSQL_BOTH.
        $returnArray= array();
        while ($row = mysqli_fetch_array($this->res, MYSQLI_ASSOC)) {
            array_push($returnArray,$row);
        }
        return $returnArray;
    }

    function get_insert_id(){
        global $conn;
        $this->insertId = @$conn->insert_id;
        return $this->insertId;
    }


    function get_cust_id(){
        global $conn;
        $result = mysqli_query($conn,"call GEN_CUSTOMER_ID(08,00)");
        $row = mysqli_fetch_array($result);

        $objUtil = new Core_PassSystem();
        $objUtil->log_data("\n abc ".$row[0]);
    }
	function getFieldNames(){
		global $conn;
		return mysqli_fetch_fields($this->res);
	}
    function getClose(){
		mysqli_close($conn);
    }
	

}

?>