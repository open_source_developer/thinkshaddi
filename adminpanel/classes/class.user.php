<?php
    class User{
        private $db;
        public $errorFlag =0;
        public $errorMessage ="";

        function  __construct() {
            $this->db = new DatabaseI_System();
        }
        
        function checkLogin($username,$password){
            $sq="select password from ts_custom_admin where userId ='".$this->db->escape_string(trim($username))."' and status='Y'";
            $res = $this->db->query($sq);
            if($res === false){
                $this->errorFlag = 1;
                $this->errorMessage = "Database Error , please try again later.";
                return false;
            }elseif($res->getNumRows() > 0){
                $userData = $res->getNext();
                $cmp = strcmp(md5($password),$userData['password']); 
                if($cmp == 0){
                    return true;
                }else{
                    $this->errorFlag = 1;
                    $this->errorMessage = "Username and Password does not match.";
                    return false;
                }
            }else{
                $this->errorFlag = 1;
                $this->errorMessage = "Username and Password does not match.";
                return false;
            }
        }

        function get_user_form_details($limit,$startrow,$orderBy,$sort,$search){
			$sq ="select cu.id,cu.firstname,cu.lastname,cu.email,DATE_FORMAT(cu.insertdatetime,'%d-%m-%Y') as datetime ,IFNULL(tu.uid,0) as uid
            from ts_custom_userform cu
            left join ts_users tu on tu.mail = cu.email";
			if(!is_null($search) && is_array($search) && count($search) > 0){
				if(isset($search['date'])){
					$sq .=" where `insertdatetime` between '".$this->db->escape_string($search['date']['from'])." 00:00:00' and '".$this->db->escape_string($search['date']['till'])." 23:59:59'";
				}
			}
			if(!empty($orderBy)){
                $sq .=" order by ".$orderBy." ".$sort;
            }else{
				$sq .=" order by `insertdatetime` desc";
			}
			if($limit > 0){
				$sq .=" limit ".$startrow.",".$limit;
			}
			
			//echo $sq;
            $res = $this->db->query($sq);
            return $res;
		}
		
		function get_user_form_details_csv($orderBy,$sort,$search){
			$sq ="select firstname as 'First Name',lastname as 'Last Name',email as 'Email Id',DATE_FORMAT(insertdatetime,'%d-%m-%Y') as 'Contact Date' from ts_custom_userform";
			if(!is_null($search) && is_array($search) && count($search) > 0){
				if(isset($search['date'])){
					$sq .=" where `insertdatetime` between '".$this->db->escape_string($search['date']['from'])." 00:00:00' and '".$this->db->escape_string($search['date']['till'])." 23:59:59'";
				}
			}
			if(!empty($orderBy)){
                $sq .=" order by ".$orderBy." ".$sort;
            }else{
				$sq .=" order by `insertdatetime` desc";
			}
			
			
			//echo $sq;
            $res = $this->db->query($sq);
            return $res;
		}

        function deleteUser($id){
            $sq ="delete from ts_custom_userform where id = '".$this->db->escape_string($id)."'";
            $res = $this->db->query($sq);
            return $res;
        }
		
		function get_user_form_count($search){
			$sq ="select count(*) as total from ts_custom_userform ";
			if(!is_null($search) && is_array($search) && count($search) > 0){
				if(isset($search['date'])){
					$sq .=" where `insertdatetime` between '".$this->db->escape_string($search['date']['from'])." 00:00:00' and '".$this->db->escape_string($search['date']['till'])." 23:59:59'";
				}
			}
			
			//echo $sq;
            $res = $this->db->query($sq);
            return $res;
		}
		
		function get_vendor_form_details($limit,$startrow,$orderBy,$sort,$search){
            $sq ="select cv.id,cv.businessname,cv.email,cv.website,DATE_FORMAT(cv.insertdatetime,'%d-%m-%Y %H:%i') as datetime,randomNo,DATE_FORMAT(mailSentDateTime,'%d-%m-%Y %H:%i') as mailSentDateTime ,IFNULL(tu.uid,0) as uid
                from ts_custom_vendorform cv
                left join ts_users tu on tu.mail = cv.email";
            if(!is_null($search) && is_array($search) && count($search) > 0){
                if(isset($search['date'])){
                    $sq .=" where `insertdatetime` between '".$this->db->escape_string($search['date']['from'])." 00:00:00' and '".$this->db->escape_string($search['date']['till'])." 23:59:59'";
                }
            }
            if(!empty($orderBy)){
                $sq .=" order by ".$orderBy." ".$sort;
            }else{
                $sq .=" order by `insertdatetime` desc";
            }
            if($limit > 0){
                $sq .=" limit ".$startrow.",".$limit;
            }
            
            //echo $sq;
            $res = $this->db->query($sq);
            return $res;
        }

        function deleteVendor($id){
            $sq ="delete from ts_custom_vendorform where id = '".$this->db->escape_string($id)."'";
            $res = $this->db->query($sq);
            return $res;
        }
        
        function get_vendor_form_details_csv($orderBy,$sort,$search){
            $sq ="select businessname as 'Business Name',email as 'Email Id',website as 'Website',DATE_FORMAT(insertdatetime,'%d-%m-%Y %H:%i') as 'Contact Date' from ts_custom_vendorform";
            if(!is_null($search) && is_array($search) && count($search) > 0){
                if(isset($search['date'])){
                    $sq .=" where `insertdatetime` between '".$this->db->escape_string($search['date']['from'])." 00:00:00' and '".$this->db->escape_string($search['date']['till'])." 23:59:59'";
                }
            }
            if(!empty($orderBy)){
                $sq .=" order by ".$orderBy." ".$sort;
            }else{
                $sq .=" order by `insertdatetime` desc";
            }
            
            
            //echo $sq;
            $res = $this->db->query($sq);
            return $res;
        }
        
        function get_vendor_form_count($search){
            $sq ="select count(*) as total from ts_custom_vendorform ";
            if(!is_null($search) && is_array($search) && count($search) > 0){
                if(isset($search['date'])){
                    $sq .=" where `insertdatetime` between '".$this->db->escape_string($search['date']['from'])." 00:00:00' and '".$this->db->escape_string($search['date']['till'])." 23:59:59'";
                }
            }
            
            //echo $sq;
            $res = $this->db->query($sq);
            return $res;
        }

      function updateVendor($email,$random){
        $sq ="update ts_custom_vendorform set randomNo = '".$random."' , mailSentDateTime = '".date('Y-m-d H:i:s')."' where email = '".$email."'";
         $res = $this->db->query($sq);
         return $res;

      }
        

    }
?>