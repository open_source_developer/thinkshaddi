<?php
//source unknown for logic of showPageNumbers()
//modified by drale.com - 1-19-2010
//added query_string reproduction and divs
//added showNext() and showPrev()

class Pagination {
	function getStartRow($page,$limit){
		$startrow = $page * $limit - ($limit);
		return $startrow;
	}	
	function showPageNumbers($totalrows,$page,$limit){
	
		$query_string = $this->queryString();
	
		$pagination_links = null;
	
		/*
		PAGINATION SCRIPT
		seperates the list into pages
		*/		
		 $numofpages = $totalrows / $limit; 
		/* We divide our total amount of rows (for example 102) by the limit (25). This 

	will yield 4.08, which we can round down to 4. In the next few lines, we'll 
	create 4 pages, and then check to see if we have extra rows remaining for a 5th 
	page. */
		
		for($i = 1; $i <= $numofpages; $i++){
		/* This for loop will add 1 to $i at the end of each pass until $i is greater 
	than $numofpages (4.08). */		
				
		  if($i == $page){
				$pagination_links .= '<div class="page-link"><span>'.$i.'</span></div> ';
			}else{
				if($query_string == ''){
					$pagination_links .= '<div class="page-link"><a href="?page='.$i.'">'.$i.'</a></div> '; 
				}else{
					$pagination_links .= '<div class="page-link"><a href="?'.$query_string.'&page='.$i.'">'.$i.'</a></div> '; 
				}
				
			}
			/* This if statement will not make the current page number available in 
	link form. It will, however, make all other pages available in link form. */
		}   // This ends the for loop
		
		if(($totalrows % $limit) != 0){
		/* The above statement is the key to knowing if there are remainders, and it's 
		all because of the %. In PHP, C++, and other languages, the % is known as a 
		Modulus. It returns the remainder after dividing two numbers. If there is no 
		remainder, it returns zero. In our example, it will return 0.8 */
			 
			if($i == $page){
				$pagination_links .= '<div class="page-link"><span>'.$i.'</span></div> ';
			}else{
				if($query_string == ''){
					$pagination_links .= '<div class="page-link"><a href="?page='.$i.'">'.$i.'</a></div> '; 
				}else{
					$pagination_links .= '<div class="page-link"><a href="?'.$query_string.'&page='.$i.'">'.$i.'</a></div> '; 
				}
				
			}
			/* This is the exact statement that turns pages into link form that is used above */ 
		}   // Ends the if statement 
	
		return $pagination_links;
	}

	//added by drale.com - 1-19-2010
	function showNext($totalrows,$page,$limit,$text="next &raquo;"){	
		$next_link = null;
		$numofpages = $totalrows / $limit;
		$query_string = $this->queryString();
		if($page < $numofpages){
			$page++;
			if($query_string == ''){
					$next_link .= '<div class="page-link"><a href="?page='.$page.'">'.$text.'</a></div>'; 
				}else{
					$next_link .= '<div class="page-link"><a href="?'.$query_string.'&page='.$page.'">'.$text.'</a></div> '; 
				}
			
		}
		
		return $next_link;
	}
	
	function showPrev($totalrows,$page,$limit,$text="&laquo; prev"){	
		$next_link = null;
		$query_string = $this->queryString();
		$numofpages = $totalrows / $limit;
		$prev_link = '';
		if($page > 1){
			$page--;
			if($query_string == ''){
				$prev_link .= '<div class="page-link"><a href="?page='.$page.'">'.$text.'</a></div>'; 
			}else{
				$prev_link .= '<div class="page-link"><a href="?'.$query_string.'&page='.$page.'">'.$text.'</a></div>'; 
			}
			
		}
		
		return $prev_link;
	}
	
	function queryString(){	
		//matches up to 10 digits in page number
		$query_string ='';
		
		if(!empty($_SERVER['QUERY_STRING'])){
		
			 $query_string = eregi_replace("page=[0-9]{0,10}","",$_SERVER['QUERY_STRING']);
		}
		
		return $query_string;
	}

        function columnSortArrows($field,$text,$currentfield=null,$currentsort=null){
                //defaults all field links to SORT ASC
                //if field link is current ORDERBY then make arrow and opposite current SORT
				//echo $currentfield."--".$currentsort;
                $sortquery = "sort=ASC";
                $orderquery = "orderby=".$field;

                if($currentsort == "ASC"){
                        $sortquery = "sort=DESC";
                        $sortarrow = '<img src="images/arrow_up.png" />';
                }

                if($currentsort == "DESC"){
                        $sortquery = "sort=ASC";
                        $sortarrow = '<img src="images/arrow_down.png" />';
                }

                if($currentfield == $field){
                        $orderquery = "orderby=".$field;
                }else{
                        $sortarrow = null;
                }
				$query_string = $this->queryString();
				if(!empty($query_string)){
		
					 $query_string = eregi_replace("&orderby=[A-Za-z]{0,20}","",$query_string );
					 $query_string = eregi_replace("orderby=[A-Za-z]{0,20}","",$query_string );
					 
				}
				if(!empty($query_string)){
		
					 $query_string = eregi_replace("&sort=DESC","",$query_string );
					 
				}
				if(!empty($query_string)){
		
					 $query_string = eregi_replace("&sort=ASC","",$query_string );
					 
				}
				if(!empty($query_string)){
		
					 $sortLink = '<a href="?'.$query_string.'&'.$orderquery.'&'.$sortquery.'" title="Click to sort the content">'.$text.'</a> '. $sortarrow;;
					 
				}else{
					$sortLink = '<a href="?'.$orderquery.'&'.$sortquery.'" title="Click to sort the content">'.$text.'</a> '. $sortarrow;;
				}
				return $sortLink;

        }
} 
?>