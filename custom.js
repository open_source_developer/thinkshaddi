/**
 * Created with JetBrains WebStorm.
 * User: Ashirwad
 * Date: 11/4/12
 * Time: 2:22 AM
 * To change this template use File | Settings | File Templates.
 */
/*jslint bitwise: true, eqeqeq: true, passfail: false, nomen: false, plusplus: false, undef: true, evil: true */
/*global window, document, $, jQuery, self, Modernizr */

// Define our root namespace if necessary
var thinkShaadi = window.thinkShaadi || {};

thinkShaadi.lightBox = {
	wWidth: 0,
	wHeight: 0,
	lightBoxStructure: '<div class="lightBoxWrapper"></div><div class="lightBoxInner"></div>',
	lightBoxWrapper: '.lightBoxWrapper',
	lightBoxInner: '.lightBoxInner',
	innerBox:"",
	lightBoxLinks: {
		open: '.linkToggleView, .linkLoin, .linkSignUp',
		close: '.btnClose, .lightBoxWrapper'
	},

	setup: function() {
		thinkShaadi.lightBox.bindEvents();
	},
	alignToMiddle: function(innerBox) {
		if (jQuery.browser.msie) {
			this.wWidth = document.documentElement.offsetWidth;
			this.wHeight = document.documentElement.offsetHeight;

		} else {
			this.wWidth = window.innerWidth;
			this.wHeight = window.innerHeight;
		}
		var innerWidth = innerBox.width(),
			innerHeight = innerBox.height(),
			calculatedLeft = (this.wWidth/2)-(innerWidth/2),
			calculatedTop = (this.wHeight/2)-(innerHeight/2);

		if(this.wHeight < innerBox.height()){
			calculatedTop = calculatedTop+50;
		}

		innerBox.css({
			top: calculatedTop+50,
			left: calculatedLeft
		});
	},

	bindEvents: function(){
		jQuery(this.lightBoxLinks.open).click(function(event) {
			event.preventDefault();
			thinkShaadi.lightBox.createLightBox(jQuery(this));
		});
		jQuery(window).bind("resize", function () {
			thinkShaadi.lightBox.alignToMiddle(jQuery(thinkShaadi.lightBox.lightBoxInner));
		});
	},

	createLightBox: function(links) {
		var url = links.attr('rel'),
			body = jQuery('body');

		body.append(thinkShaadi.lightBox.lightBoxStructure);

		jQuery(thinkShaadi.lightBox.lightBoxWrapper).fadeIn(function(){
			jQuery(window).trigger('resize');
			jQuery(thinkShaadi.lightBox.lightBoxInner).fadeIn();

		});
		if(url.indexOf('img') === -1) {
			thinkShaadi.lightBox.alignToMiddle(jQuery(thinkShaadi.lightBox.lightBoxInner).addClass('lightBoxInnerPages').load(url));
		} else {
			thinkShaadi.lightBox.alignToMiddle(jQuery(thinkShaadi.lightBox.lightBoxInner).html('<img height="500" src="'+url+'" />'));
		}

		//set up lightBox close functionality
		jQuery(this.lightBoxLinks.close).live('click', function(){
			thinkShaadi.lightBox.closeLightBox();
		});

		body.keydown(function(event) {
			if (event.which == 27) {
				thinkShaadi.lightBox.closeLightBox();
			}
		});

	},
	closeLightBox: function() {
		jQuery(thinkShaadi.lightBox.lightBoxInner).fadeOut(function() {
			jQuery(thinkShaadi.lightBox.lightBoxWrapper).fadeOut('slow', function(){
				jQuery(this).remove()
			});
			jQuery(thinkShaadi.lightBox.lightBoxInner).remove()
		});
		jQuery('body').removeClass('lightBoxActive');
	},
	init: function() {
		//thinkShaadi.lightBox.setup();
	}
};

thinkShaadi.tabs = {
	commonTabs: function (tabWrapper){
		var tabs = jQuery(tabWrapper).find('.tabs li a'),
			tabContent = jQuery(tabWrapper).find('.tabContent > li');
		jQuery('.tabContent > li').hide().eq(0).show();
		jQuery(tabs).each(function(){
			jQuery(this).click(function(e){
				e.preventDefault();
				if(jQuery(this).parent().hasClass('active')){
					return false;
				} else {
					jQuery('.tabs li').removeClass('active');
					jQuery(this).parent().addClass('active');
					var idx = jQuery(this).parent().index();
					jQuery('.tabContent > li').hide();
					jQuery(tabContent).eq(idx).show();
				}
			})
		})
	},
	init: function(){
		if(jQuery('.fnTabs').length !== 0){
			thinkShaadi.tabs.commonTabs('.fnTabs');
		}
	}
};

thinkShaadi.equalHeight = {
	calculateHeight: function(parent){
		console.log(jQuery(parent).length);
		if(jQuery(parent).length !== 0){
			jQuery(parent).css('border','4px solid red');
		}
	},
	init: function(){
		jQuery('.linkLoin').click(function(){
			thinkShaadi.equalHeight.calculateHeight('.fnEqualHeight');
		});

	}
};

thinkShaadi.selectBoxStyle = {
	init: function(){
		jQuery("select").selectBoxIt({

			//theme
			theme: 'jqueryui',

			// Uses the jQuery 'fadeIn' effect when opening the drop down
			showEffect: "fadeIn",

			// Sets the jQuery 'fadeIn' effect speed to 400 milleseconds
			showEffectSpeed: 400,

			// Uses the jQuery 'fadeOut' effect when closing the drop down
			hideEffect: "fadeOut",

			// Sets the jQuery 'fadeOut' effect speed to 400 milleseconds
			hideEffectSpeed: 400

		});
	}
};

thinkShaadi.formAddMore = {
	addMoreButton: '.btnAddMore',
	removeEntryButton: '.removeEntry',
	appendData : function(data){
		var newVal = data.prev('input').val();
		if(newVal !== ''){
			data.siblings('.newEntry').append('<li>'+newVal+'<a href="#" class="removeEntry">Remove</a></li>');
		}
	},
	removeData: function(data){
		data.parent('li').remove();
	},
	setup: function(){
		thinkShaadi.formAddMore.bindEvents();
	},
	bindEvents: function () {
		jQuery(this.addMoreButton).live('click', function(event){
			event.preventDefault();
			thinkShaadi.formAddMore.appendData(jQuery(this));
		})
		jQuery(this.removeEntryButton).live('click', function(event){
			event.preventDefault();
			thinkShaadi.formAddMore.removeData(jQuery(this))
		})
	},
	init: function() {
		thinkShaadi.formAddMore.setup();
	}
}
/*
 * SETUP DOM READY AND LOAD EVENTS HERE
 */
jQuery(document).ready(function () {
	//jQuery('.loadMoreAjaxLoader').hide();
	if(jQuery('.categoryResults').length !== 0){
		thinkShaadi.createThumbs.init();
	}
	thinkShaadi.footer.init();
//      thinkShaadi.heroCarousal.init();
	if(jQuery('.fnTabs').length !== 0){
		thinkShaadi.tabs.init();
	}
	thinkShaadi.formAddMore.init();
	//thinkShaadi.selectBoxStyle.init();
	jQuery('.categoryWrapper ul li a').click(function(){
		jQuery('.loadMoreAjaxLoader').text('').append('<img src="/sites/all/themes/thinkshaadi/assets/img/loader.gif" />');
		start = 0;
		if(jQuery(this).parents('div').hasClass('subMenu')) {
			jQuery('.categoryWrapper ul li li').removeClass('active');
		} else {
			jQuery('.categoryWrapper ul li').removeClass('active');
		}
		jQuery(this).parent('li').addClass('active');
		if(jQuery(this).hasClass('catCategory')){
			thinkShaadi.createCategoryThumbs.init();
		} else {
			jQuery('.categoryResults > .categoryList').remove();
			jQuery('.hasSubMenu').animate({
				'paddingBottom': 60
			});
			jQuery('.subMenu').fadeIn().find('li').eq(0).addClass('active');

			jQuery('.categoryResults').append('<ul class="thumbList"></ul>');
			thinkShaadi.createThumbs.init();
		}
	})
});